<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Pemeriksaans;
use Faker\Generator as Faker;

$factory->define(Pemeriksaans::class, function (Faker $faker) {

    return [
        'pasien_id' => $faker->randomDigitNotNull,
        'tgl_pemeriksaan' => $faker->word,
        'keluhan' => $faker->word,
        'anamnese' => $faker->word,
        'diagnosa' => $faker->word,
        'pemberian_obat' => $faker->word,
        'dosis' => $faker->word,
        'dokter_id' => $faker->randomDigitNotNull,
        'tarif_jasa_dokter' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
