<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Dokter;
use Faker\Generator as Faker;

$factory->define(Dokter::class, function (Faker $faker) {

    return [
        'nama_dokter' => $faker->word,
        'jabatan' => $faker->word,
        'alamat' => $faker->word,
        'no_hp' => $faker->word,
        'created_at' => $faker->word,
        'updated_at' => $faker->word,
        'deleted_at' => $faker->word
    ];
});
