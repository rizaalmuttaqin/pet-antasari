<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Pendaftarans;
use Faker\Generator as Faker;

$factory->define(Pendaftarans::class, function (Faker $faker) {

    return [
        'no_antrian' => $faker->word,
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'pasien_id' => $faker->randomDigitNotNull
    ];
});
