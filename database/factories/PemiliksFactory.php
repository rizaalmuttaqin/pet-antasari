<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Pemiliks;
use Faker\Generator as Faker;

$factory->define(Pemiliks::class, function (Faker $faker) {

    return [
        'nama_pemilik' => $faker->word,
        'alamat_pemilik' => $faker->word,
        'no_hp' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
