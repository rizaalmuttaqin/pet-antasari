<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Transaksis;
use Faker\Generator as Faker;

$factory->define(Transaksis::class, function (Faker $faker) {

    return [
        'anamnese' => $faker->word,
        'pemberian_obat' => $faker->word,
        'dosis' => $faker->word,
        'tarif_jasa_dokter' => $faker->word,
        'created_at' => $faker->word,
        'updated_at' => $faker->word,
        'deleted_at' => $faker->word,
        'pasien_id' => $faker->randomDigitNotNull
    ];
});
