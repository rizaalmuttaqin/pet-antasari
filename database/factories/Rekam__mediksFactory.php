<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Rekam__mediks;
use Faker\Generator as Faker;

$factory->define(Rekam__mediks::class, function (Faker $faker) {

    return [
        'pasien_id' => $faker->randomDigitNotNull,
        'diagnosa' => $faker->word,
        'dokter_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'pemeriksaan_id' => $faker->randomDigitNotNull
    ];
});
