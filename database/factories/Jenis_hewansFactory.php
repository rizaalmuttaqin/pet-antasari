<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Jenis_hewans;
use Faker\Generator as Faker;

$factory->define(Jenis_hewans::class, function (Faker $faker) {

    return [
        'jenis_hewan' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
