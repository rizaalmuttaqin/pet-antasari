<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Obats;
use Faker\Generator as Faker;

$factory->define(Obats::class, function (Faker $faker) {

    return [
        'nama_obat' => $faker->word,
        'jenis_obat' => $faker->word,
        'jumlah' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'satuan_id' => $faker->randomDigitNotNull
    ];
});
