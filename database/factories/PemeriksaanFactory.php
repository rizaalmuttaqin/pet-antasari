<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Pemeriksaan;
use Faker\Generator as Faker;

$factory->define(Pemeriksaan::class, function (Faker $faker) {

    return [
        'pasien_id' => $faker->randomDigitNotNull,
        'pemilk' => $faker->word,
        'tgl_pemeriksaan' => $faker->word,
        'anamnese' => $faker->word,
        'diagnosa' => $faker->word,
        'tindakan' => $faker->word,
        'Foto' => $faker->word,
        'pemberian_obat' => $faker->word,
        'dosis' => $faker->word,
        'dokter_id' => $faker->randomDigitNotNull,
        'tarif_jasa_dokter' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
