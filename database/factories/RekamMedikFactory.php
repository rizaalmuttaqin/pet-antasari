<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\RekamMedik;
use Faker\Generator as Faker;

$factory->define(RekamMedik::class, function (Faker $faker) {

    return [
        'pasien_id' => $faker->randomDigitNotNull,
        'diagnosa' => $faker->word,
        'dokter_id' => $faker->randomDigitNotNull,
        'tgl_pemeriksaan' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'pemeriksaan_id' => $faker->randomDigitNotNull
    ];
});
