<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Pasiens;
use Faker\Generator as Faker;

$factory->define(Pasiens::class, function (Faker $faker) {

    return [
        'nama_pasien' => $faker->word,
        'pemilik_id' => $faker->randomDigitNotNull,
        'berat_badan' => $faker->word,
        'jenis_kelamin' => $faker->word,
        'ras' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'jenis_hewan_id' => $faker->randomDigitNotNull
    ];
});
