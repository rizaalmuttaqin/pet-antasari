<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Laporan;
use Faker\Generator as Faker;

$factory->define(Laporan::class, function (Faker $faker) {

    return [
        'id' => $faker->randomDigitNotNull,
        'id_pemilik' => $faker->randomDigitNotNull,
        'id_pemeriksaan' => $faker->randomDigitNotNull,
        'id_dokter' => $faker->randomDigitNotNull,
        'id_user' => $faker->randomDigitNotNull,
        'id_transaksi' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
