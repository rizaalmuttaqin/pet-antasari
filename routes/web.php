<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index');







Route::resource('dokters', 'DoktersController');

Route::resource('pemiliks', 'PemiliksController');

Route::resource('pendaftarans', 'PendaftaransController');

Route::resource('pasiens', 'PasiensController');

Route::resource('pemeriksaans', 'PemeriksaanController');

Route::resource('jenisHewans', 'Jenis_hewansController');

Route::resource('transakses', 'TransaksisController');

Route::resource('satuans', 'SatuansController');

Route::resource('obats', 'ObatsController');



Route::resource('rekamMediks', 'RekamMedikController');

Route::resource('pemiliks', 'PemiliksController');

Route::resource('pemiliks', 'PemiliksController');

Route::resource('laporans', 'LaporanController');

Route::get('laporan_jasa_dokter','PemeriksaanController@laporanJasaDokter');