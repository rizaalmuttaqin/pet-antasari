<?php

namespace App\Repositories;

use App\Models\Transaksis;
use App\Repositories\BaseRepository;

/**
 * Class TransaksisRepository
 * @package App\Repositories
 * @version December 21, 2019, 2:08 pm UTC
*/

class TransaksisRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'anamnese',
        'pemberian_obat',
        'dosis',
        'tarif_jasa_dokter',
        'pasien_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Transaksis::class;
    }
}
