<?php

namespace App\Repositories;

use App\Models\Pendaftarans;
use App\Repositories\BaseRepository;

/**
 * Class PendaftaransRepository
 * @package App\Repositories
 * @version December 21, 2019, 1:40 pm UTC
*/

class PendaftaransRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'no_antrian',
        'pasien_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Pendaftarans::class;
    }
}
