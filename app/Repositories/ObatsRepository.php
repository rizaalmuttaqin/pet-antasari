<?php

namespace App\Repositories;

use App\Models\Obats;
use App\Repositories\BaseRepository;

/**
 * Class ObatsRepository
 * @package App\Repositories
 * @version December 21, 2019, 2:10 pm UTC
*/

class ObatsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama_obat',
        'jenis_obat',
        'jumlah',
        'satuan_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Obats::class;
    }
}
