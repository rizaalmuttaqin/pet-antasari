<?php

namespace App\Repositories;

use App\Models\Pemeriksaan;
use App\Repositories\BaseRepository;

/**
 * Class PemeriksaanRepository
 * @package App\Repositories
 * @version April 22, 2020, 9:50 am UTC
*/

class PemeriksaanRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'pasien_id',
        'pemilk',
        'tgl_pemeriksaan',
        'anamnese',
        'diagnosa',
        'tindakan',
        'file',
        'pemberian_obat',
        'dosis',
        'dokter_id',
        'tarif_jasa_dokter'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Pemeriksaan::class;
    }
}
