<?php

namespace App\Repositories;

use App\Models\RekamMedik;
use App\Repositories\BaseRepository;

/**
 * Class RekamMedikRepository
 * @package App\Repositories
 * @version February 25, 2020, 6:46 am UTC
*/

class RekamMedikRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'pasien_id',
        'diagnosa',
        'dokter_id',
        'tgl_pemeriksaan',
        'pemeriksaan_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RekamMedik::class;
    }
}
