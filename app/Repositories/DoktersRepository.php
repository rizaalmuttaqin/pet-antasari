<?php

namespace App\Repositories;

use App\Models\Dokters;
use App\Repositories\BaseRepository;

/**
 * Class DoktersRepository
 * @package App\Repositories
 * @version December 18, 2019, 6:03 am UTC
*/

class DoktersRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama_dokter',
        'jabatan',
        'alamat',
        'no_hp'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Dokters::class;
    }
}
