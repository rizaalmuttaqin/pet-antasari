<?php

namespace App\Repositories;

use App\Models\Satuans;
use App\Repositories\BaseRepository;

/**
 * Class SatuansRepository
 * @package App\Repositories
 * @version December 21, 2019, 2:09 pm UTC
*/

class SatuansRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama',
        'keterangan'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Satuans::class;
    }
}
