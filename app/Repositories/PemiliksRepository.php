<?php

namespace App\Repositories;

use App\Models\Pemiliks;
use App\Repositories\BaseRepository;

/**
 * Class PemiliksRepository
 * @package App\Repositories
 * @version March 13, 2020, 7:09 am UTC
*/

class PemiliksRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama_pemilik',
        'alamat_pemilik',
        'no_hp'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Pemiliks::class;
    }
}
