<?php

namespace App\Repositories;

use App\Models\Laporan;
use App\Repositories\BaseRepository;

/**
 * Class LaporanRepository
 * @package App\Repositories
 * @version September 11, 2020, 1:14 pm UTC
*/

class LaporanRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'id_pemilik',
        'id_pemeriksaan',
        'id_dokter',
        'id_user',
        'id_transaksi'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Laporan::class;
    }
}
