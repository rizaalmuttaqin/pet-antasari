<?php

namespace App\Repositories;

use App\Models\Pasiens;
use App\Repositories\BaseRepository;

/**
 * Class PasiensRepository
 * @package App\Repositories
 * @version December 21, 2019, 1:49 pm UTC
*/

class PasiensRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama_pasien',
        'file',
        'nama_pemilik',
        'alamat',
        'no_hp',
        'berat_badan',
        'jenis_kelamin',
        'ras',
        'jenis_hewan_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Pasiens::class;
    }
}
