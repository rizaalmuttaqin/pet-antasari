<?php

namespace App\Repositories;

use App\Models\Jenis_hewans;
use App\Repositories\BaseRepository;

/**
 * Class Jenis_hewansRepository
 * @package App\Repositories
 * @version December 21, 2019, 1:55 pm UTC
*/

class Jenis_hewansRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'jenis_hewan'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Jenis_hewans::class;
    }
}
