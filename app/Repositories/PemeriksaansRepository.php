<?php

namespace App\Repositories;

use App\Models\Pemeriksaan;
use App\Repositories\BaseRepository;

/**
 * Class PemeriksaansRepository
 * @package App\Repositories
 * @version December 21, 2019, 1:53 pm UTC
*/

class PemeriksaansRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'pasien_id',
        'pemilik_id',
        'tgl_pemeriksaan',
        'keluhan',
        'anamnese',
        'file',
        'diagnosa',
        'pemberian_obat',
        'dosis',
        'dokter_id',
        'tarif_jasa_dokter'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Pemeriksaan::class;
    }
}
