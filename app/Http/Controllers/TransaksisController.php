<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTransaksisRequest;
use App\Http\Requests\UpdateTransaksisRequest;
use App\Repositories\TransaksisRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Models\Pasiens;

class TransaksisController extends AppBaseController
{
    /** @var  TransaksisRepository */
    private $transaksisRepository;

    public function __construct(TransaksisRepository $transaksisRepo)
    {
        $this->transaksisRepository = $transaksisRepo;
    }

    /**
     * Display a listing of the Transaksis.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $transakses = $this->transaksisRepository->all();

        return view('transakses.index')
            ->with('transakses', $transakses);
    }

    /**
     * Show the form for creating a new Transaksis.
     *
     * @return Response
     */
    public function create()
    {
        $pasien = Pasiens::pluck('nama_pasien','id');
        return view('transakses.create',compact('pasien'));
    }

    /**
     * Store a newly created Transaksis in storage.
     *
     * @param CreateTransaksisRequest $request
     *
     * @return Response
     */
    public function store(CreateTransaksisRequest $request)
    {
        $input = $request->all();

        $transaksis = $this->transaksisRepository->create($input);

        Flash::success('Transaksis saved successfully.');

        return redirect(route('transakses.index'));
    }

    /**
     * Display the specified Transaksis.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $transaksis = $this->transaksisRepository->find($id);

        if (empty($transaksis)) {
            Flash::error('Transaksis not found');

            return redirect(route('transakses.index'));
        }

        return view('transakses.show')->with('transaksis', $transaksis);
    }

    /**
     * Show the form for editing the specified Transaksis.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $transaksis = $this->transaksisRepository->find($id);

        if (empty($transaksis)) {
            Flash::error('Transaksis not found');

            return redirect(route('transakses.index'));
        }

        return view('transakses.edit')->with('transaksis', $transaksis);
    }

    /**
     * Update the specified Transaksis in storage.
     *
     * @param int $id
     * @param UpdateTransaksisRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTransaksisRequest $request)
    {
        $transaksis = $this->transaksisRepository->find($id);

        if (empty($transaksis)) {
            Flash::error('Transaksis not found');

            return redirect(route('transakses.index'));
        }

        $transaksis = $this->transaksisRepository->update($request->all(), $id);

        Flash::success('Transaksis updated successfully.');

        return redirect(route('transakses.index'));
    }

    /**
     * Remove the specified Transaksis from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $transaksis = $this->transaksisRepository->find($id);

        if (empty($transaksis)) {
            Flash::error('Transaksis not found');

            return redirect(route('transakses.index'));
        }

        $this->transaksisRepository->delete($id);

        Flash::success('Transaksis deleted successfully.');

        return redirect(route('transakses.index'));
    }
}
