<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSatuansRequest;
use App\Http\Requests\UpdateSatuansRequest;
use App\Repositories\SatuansRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class SatuansController extends AppBaseController
{
    /** @var  SatuansRepository */
    private $satuansRepository;

    public function __construct(SatuansRepository $satuansRepo)
    {
        $this->satuansRepository = $satuansRepo;
    }

    /**
     * Display a listing of the Satuans.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $satuans = $this->satuansRepository->all();

        return view('satuans.index')
            ->with('satuans', $satuans);
    }

    /**
     * Show the form for creating a new Satuans.
     *
     * @return Response
     */
    public function create()
    {
        return view('satuans.create');
    }

    /**
     * Store a newly created Satuans in storage.
     *
     * @param CreateSatuansRequest $request
     *
     * @return Response
     */
    public function store(CreateSatuansRequest $request)
    {
        $input = $request->all();

        $satuans = $this->satuansRepository->create($input);

        Flash::success('Satuans saved successfully.');

        return redirect(route('satuans.index'));
    }

    /**
     * Display the specified Satuans.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $satuans = $this->satuansRepository->find($id);

        if (empty($satuans)) {
            Flash::error('Satuans not found');

            return redirect(route('satuans.index'));
        }

        return view('satuans.show')->with('satuans', $satuans);
    }

    /**
     * Show the form for editing the specified Satuans.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $satuans = $this->satuansRepository->find($id);

        if (empty($satuans)) {
            Flash::error('Satuans not found');

            return redirect(route('satuans.index'));
        }

        return view('satuans.edit')->with('satuans', $satuans);
    }

    /**
     * Update the specified Satuans in storage.
     *
     * @param int $id
     * @param UpdateSatuansRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSatuansRequest $request)
    {
        $satuans = $this->satuansRepository->find($id);

        if (empty($satuans)) {
            Flash::error('Satuans not found');

            return redirect(route('satuans.index'));
        }

        $satuans = $this->satuansRepository->update($request->all(), $id);

        Flash::success('Satuans updated successfully.');

        return redirect(route('satuans.index'));
    }

    /**
     * Remove the specified Satuans from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $satuans = $this->satuansRepository->find($id);

        if (empty($satuans)) {
            Flash::error('Satuans not found');

            return redirect(route('satuans.index'));
        }

        $this->satuansRepository->delete($id);

        Flash::success('Satuans deleted successfully.');

        return redirect(route('satuans.index'));
    }
}
