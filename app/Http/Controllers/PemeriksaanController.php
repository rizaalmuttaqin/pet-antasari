<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePemeriksaanRequest;
use App\Http\Requests\UpdatePemeriksaanRequest;
use App\Repositories\PemeriksaanRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Models\Dokters;
use App\Models\Pasiens;
use App\Models\Pemeriksaan;
use Flash;
use File;
use Response;
use DB;

class PemeriksaanController extends AppBaseController
{
    /** @var  PemeriksaanRepository */
    private $pemeriksaanRepository;

    public function __construct(PemeriksaanRepository $pemeriksaanRepo)
    {
        $this->pemeriksaanRepository = $pemeriksaanRepo;
    }

    /**
     * Display a listing of the Pemeriksaan.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $pemeriksaans = $this->pemeriksaanRepository->all();

        return view('pemeriksaans.index')
            ->with('pemeriksaans', $pemeriksaans);
    }

    /**
     * Show the form for creating a new Pemeriksaan.
     *
     * @return Response
     */
    public function create()
    {
        $pasien = Pasiens::pluck('nama_pasien','id');
        $pemilik = Pasiens::pluck('nama_pemilik','id');
        $dokter = Dokters::pluck('nama_dokter', 'id');
        return view('pemeriksaans.create', compact("pasien", "dokter", "pemilik"));

    }

    /**
     * Store a newly created Pemeriksaan in storage.
     *
     * @param CreatePemeriksaanRequest $request
     *
     * @return Response
     */
    public function store(CreatePemeriksaanRequest $request)
    {
        $input = $request->except('file');
        if( $request->hasFile('file')) {
            $file = $request->file('file');
            $filename = uniqid() . '_' . trim($file->getClientOriginalName());
            $path=$request->file->storeAs('public/foto', $filename,'local');
            $path='storage'.substr($path, strpos($path,'/'));
            $input['file']=$path;
        }
        $pemeriksaan = $this->pemeriksaanRepository->create($input);

        Flash::success('Pemeriksaan saved successfully.');
        return redirect(route('pemeriksaans.index'));
    }

    /**
     * Display the specified Pemeriksaan.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $pemeriksaan = $this->pemeriksaanRepository->find($id);

        if (empty($pemeriksaan)) {
            Flash::error('Pemeriksaan not found');

            return redirect(route('pemeriksaans.index'));
        }

        return view('pemeriksaans.show')->with('pemeriksaan', $pemeriksaan);
    }

    /**
     * Show the form for editing the specified Pemeriksaan.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $pemeriksaan = $this->pemeriksaanRepository->find($id);
        if (empty($pemeriksaan)) {
            Flash::error('Pemeriksaan not found');
            return redirect(route('pemeriksaans.index'));
        }
        $pasien = Pasiens::pluck('nama_pasien','id');
        $pemilik = Pasiens::pluck('nama_pemilik','id');
        $dokter = Dokters::pluck('nama_dokter', 'id');
        return view('pemeriksaans.edit', compact("pasien", "pemilik", "dokter"))->with('pemeriksaan', $pemeriksaan);
    }

    /**
     * Update the specified Pemeriksaan in storage.
     *
     * @param int $id
     * @param UpdatePemeriksaanRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePemeriksaanRequest $request)
    {
        $input = $request->except('file');
        $pemeriksaan = $this->pemeriksaanRepository->find($id);
        if (empty($pemeriksaan)) {
            Flash::error('Pemeriksaan not found');
            return redirect(route('pemeriksaans.index'));
        }
        if($request->hasFile('file')) {
            $result = File::exists($pemeriksaan->file);
            if($result) {
                File::Delete($pemeriksaan->file);
            }
            $file = $request->file('file');
            $filename = uniqid() . '_' . trim($file->getClientOriginalName());
            $path=$request->file->storeAs('public/foto', $filename,'local');
            $path='storage'.substr($path, strpos($path,'/'));
            $input['file']=$path;
        }
        $pemeriksaan = $this->pemeriksaanRepository->update($input, $id);
        Flash::success('Pemeriksaan updated successfully.');
        return redirect(route('pemeriksaans.index'));
    }

    /**
     * Remove the specified Pemeriksaan from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $pemeriksaan = $this->pemeriksaanRepository->find($id);

        if (empty($pemeriksaan)) {
            Flash::error('Pemeriksaan not found');

            return redirect(route('pemeriksaans.index'));
        }

        $this->pemeriksaanRepository->delete($id);

        Flash::success('Pemeriksaan deleted successfully.');

        return redirect(route('pemeriksaans.index'));
    }

    public function laporanJasaDokter(Request $request)
    {
        $input = $request->all();
        if(!empty($input)){
            if(!empty($bulan) && !empty($tahun)){
                $pemeriksaans = Pemeriksaan::select(DB::raw('sum(tarif_jasa_dokter) as pendapatan,
                count(*) as jumlah_pasien, dokter_id'))
                ->whereMonth('created_at',$input->bulan)
                ->whereYear('created_at',$input->tahun)
                ->groupBy('dokter_id')
                ->get();

                return view('laporan_jasa_dokter.index',compact('pemeriksaans'));
            }
        }

        $pemeriksaans = Pemeriksaan::select(DB::raw('sum(tarif_jasa_dokter) as pendapatan,
        count(*) as jumlah_pasien, dokter_id'))
        ->groupBy('dokter_id')
        ->get();
        return view('laporan_jasa_dokter.index',compact('pemeriksaans'));
    }
}
