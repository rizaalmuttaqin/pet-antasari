<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePasiensRequest;
use App\Http\Requests\UpdatePasiensRequest;
use App\Repositories\PasiensRepository;
use App\Repositories\PemiliksRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use File;
use Response;
use App\Models\Jenis_hewans;
use App\Models\Pasiens;
use Carbon\Carbon;
use DB;

class PasiensController extends AppBaseController
{
    /** @var  PasiensRepository */
    private $pasiensRepository;

    public function __construct(PasiensRepository $pasiensRepo)
    {
        $this->pasiensRepository = $pasiensRepo;
    }

    /**
     * Display a listing of the Pasiens.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $pasiens = $this->pasiensRepository->all();

        return view('pasiens.index')
            ->with('pasiens', $pasiens);
    }

    /**
     * Show the form for creating a new Pasiens.
     *
     * @return Response
     */
    public function create()
    {
        $jenis_hewan = Jenis_hewans::pluck('jenis_hewan','id');
        return view('pasiens.create', compact('jenis_hewan'));
    }

    /**
     * Store a newly created Pasiens in storage.
     *
     * @param CreatePasiensRequest $request
     *
     * @return Response
     */
    public function store(CreatePasiensRequest $request)
    {
        $input = $request->except('file');
        $date= Carbon::now()->format('Y_m_d');
        if($request->hasFile('file')) {
            $file = $request->file('file');
            $filename = str_replace(" ", "_",$file->getClientOriginalName());
            $filenames = $date.'_'.$filename;
            $path=$request->file->storeAs('public/foto', $filenames,'local');
            $input['file']= $filenames;
        }
        // return $request;
        $pasiens = $this->pasiensRepository->create($input);

        Flash::success('Pasiens saved successfully.');
        return redirect(route('pasiens.index'));    }

    /**
     * Display the specified Pasiens.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $pasiens = $this->pasiensRepository->find($id);

        if (empty($pasiens)) {
            Flash::error('Pasiens not found');

            return redirect(route('pasiens.index'));
        }

        return view('pasiens.show')->with('pasiens', $pasiens);
    }

    /**
     * Show the form for editing the specified Pasiens.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $pasiens = $this->pasiensRepository->find($id);
        $jenis_hewan = Jenis_hewans::pluck('jenis_hewan','id');
        
        if (empty($pasiens)) {
            Flash::error('Pasiens not found');

            return redirect(route('pasien.index'));
        }
        return view('pasiens.edit',compact('jenis_hewan'))->with('pasiens', $pasiens);
    }

    /**
     * Update the specified Pasiens in storage.
     *
     * @param int $id
     * @param UpdatePasiensRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePasiensRequest $request)
    {
        $input = $request->except('file');
        $pasiens = $this->pasiensRepository->find($id);
        if (empty($pasiens)) {
            Flash::error('Pasiens not found');
            return redirect(route('pasiens.index'));
        }
        $date= Carbon::now()->format('Y_m_d');
        if($request->hasFile('file')) {
            $result = File::exists($pasiens->file);
            if($result) {
                File::Delete($pasiens->file);
            }
            $file = $request->file('file');
            $filename = str_replace(" ", "_",$file->getClientOriginalName());
            $filenames = $date.'_'.$filename;
            $path=$request->file->storeAs('public/foto', $filenames,'local');
            $input['file']= $filenames;
        }
        $pasiens = $this->pasiensRepository->update($input, $id);
        Flash::success('Pasiens updated successfully.');
        return redirect(route('pasiens.index'));
    }

    /**
     * Remove the specified Pasiens from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $pasiens = $this->pasiensRepository->find($id);

        if (empty($pasiens)) {
            Flash::error('Pasiens not found');

            return redirect(route('pasiens.index'));
        }

        $this->pasiensRepository->delete($id);

        Flash::success('Pasiens deleted successfully.');

        return redirect(route('pasiens.index'));
    }
}
