<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDoktersRequest;
use App\Http\Requests\UpdateDoktersRequest;
use App\Repositories\DoktersRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class DoktersController extends AppBaseController
{
    /** @var  DoktersRepository */
    private $doktersRepository;

    public function __construct(DoktersRepository $doktersRepo)
    {
        $this->doktersRepository = $doktersRepo;
    }

    /**
     * Display a listing of the Dokters.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $dokters = $this->doktersRepository->all();

        return view('dokters.index')
            ->with('dokters', $dokters);
    }

    /**
     * Show the form for creating a new Dokters.
     *
     * @return Response
     */
    public function create()
    {
        return view('dokters.create');
    }

    /**
     * Store a newly created Dokters in storage.
     *
     * @param CreateDoktersRequest $request
     *
     * @return Response
     */
    public function store(CreateDoktersRequest $request)
    {
        $input = $request->all();

        $dokters = $this->doktersRepository->create($input);

        Flash::success('Dokters saved successfully.');

        return redirect(route('dokters.index'));
    }

    /**
     * Display the specified Dokters.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $dokters = $this->doktersRepository->find($id);

        if (empty($dokters)) {
            Flash::error('Dokters not found');

            return redirect(route('dokters.index'));
        }

        return view('dokters.show')->with('dokters', $dokters);
    }

    /**
     * Show the form for editing the specified Dokters.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $dokters = $this->doktersRepository->find($id);

        if (empty($dokters)) {
            Flash::error('Dokters not found');

            return redirect(route('dokters.index'));
        }

        return view('dokters.edit')->with('dokters', $dokters);
    }

    /**
     * Update the specified Dokters in storage.
     *
     * @param int $id
     * @param UpdateDoktersRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDoktersRequest $request)
    {
        $dokters = $this->doktersRepository->find($id);

        if (empty($dokters)) {
            Flash::error('Dokters not found');

            return redirect(route('dokters.index'));
        }

        $dokters = $this->doktersRepository->update($request->all(), $id);

        Flash::success('Dokters updated successfully.');

        return redirect(route('dokters.index'));
    }

    /**
     * Remove the specified Dokters from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $dokters = $this->doktersRepository->find($id);

        if (empty($dokters)) {
            Flash::error('Dokters not found');

            return redirect(route('dokters.index'));
        }

        $this->doktersRepository->delete($id);

        Flash::success('Dokters deleted successfully.');

        return redirect(route('dokters.index'));
    }
}
