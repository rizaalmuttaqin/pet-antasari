<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePendaftaransRequest;
use App\Http\Requests\UpdatePendaftaransRequest;
use App\Repositories\PendaftaransRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Models\Pasiens;

class PendaftaransController extends AppBaseController
{
    /** @var  PendaftaransRepository */
    private $pendaftaransRepository;

    public function __construct(PendaftaransRepository $pendaftaransRepo)
    {
        $this->pendaftaransRepository = $pendaftaransRepo;
    }

    /**
     * Display a listing of the Pendaftarans.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $pendaftarans = $this->pendaftaransRepository->all();

        return view('pendaftarans.index')
            ->with('pendaftarans', $pendaftarans);
    }

    /**
     * Show the form for creating a new Pendaftarans.
     *
     * @return Response
     */
    public function create()
    {
        $pasien = Pasiens::pluck('nama_pasien','id');
        return view('pendaftarans.create', compact('pasien'));
    }

    /**
     * Store a newly created Pendaftarans in storage.
     *
     * @param CreatePendaftaransRequest $request
     *
     * @return Response
     */
    public function store(CreatePendaftaransRequest $request)
    {
        $input = $request->all();

        $pendaftarans = $this->pendaftaransRepository->create($input);

        Flash::success('Pendaftarans saved successfully.');

        return redirect(route('pendaftarans.index'));
    }

    /**
     * Display the specified Pendaftarans.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $pendaftarans = $this->pendaftaransRepository->find($id);

        if (empty($pendaftarans)) {
            Flash::error('Pendaftarans not found');

            return redirect(route('pendaftarans.index'));
        }

        return view('pendaftarans.show')->with('pendaftarans', $pendaftarans);
    }

    /**
     * Show the form for editing the specified Pendaftarans.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $pendaftarans = $this->pendaftaransRepository->find($id);

        if (empty($pendaftarans)) {
            Flash::error('Pendaftarans not found');

            return redirect(route('pendaftarans.index'));
        }

        return view('pendaftarans.edit')->with('pendaftarans', $pendaftarans);
    }

    /**
     * Update the specified Pendaftarans in storage.
     *
     * @param int $id
     * @param UpdatePendaftaransRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePendaftaransRequest $request)
    {
        $pendaftarans = $this->pendaftaransRepository->find($id);

        if (empty($pendaftarans)) {
            Flash::error('Pendaftarans not found');

            return redirect(route('pendaftarans.index'));
        }

        $pendaftarans = $this->pendaftaransRepository->update($request->all(), $id);

        Flash::success('Pendaftarans updated successfully.');

        return redirect(route('pendaftarans.index'));
    }

    /**
     * Remove the specified Pendaftarans from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $pendaftarans = $this->pendaftaransRepository->find($id);

        if (empty($pendaftarans)) {
            Flash::error('Pendaftarans not found');

            return redirect(route('pendaftarans.index'));
        }

        $this->pendaftaransRepository->delete($id);

        Flash::success('Pendaftarans deleted successfully.');

        return redirect(route('pendaftarans.index'));
    }
}
