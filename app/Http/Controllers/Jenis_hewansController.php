<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateJenis_hewansRequest;
use App\Http\Requests\UpdateJenis_hewansRequest;
use App\Repositories\Jenis_hewansRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class Jenis_hewansController extends AppBaseController
{
    /** @var  Jenis_hewansRepository */
    private $jenisHewansRepository;

    public function __construct(Jenis_hewansRepository $jenisHewansRepo)
    {
        $this->jenisHewansRepository = $jenisHewansRepo;
    }

    /**
     * Display a listing of the Jenis_hewans.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $jenisHewans = $this->jenisHewansRepository->all();

        return view('jenis_hewans.index')
            ->with('jenisHewans', $jenisHewans);
    }

    /**
     * Show the form for creating a new Jenis_hewans.
     *
     * @return Response
     */
    public function create()
    {
        return view('jenis_hewans.create');
    }

    /**
     * Store a newly created Jenis_hewans in storage.
     *
     * @param CreateJenis_hewansRequest $request
     *
     * @return Response
     */
    public function store(CreateJenis_hewansRequest $request)
    {
        $input = $request->all();

        $jenisHewans = $this->jenisHewansRepository->create($input);

        Flash::success('Jenis Hewans saved successfully.');

        return redirect(route('jenisHewans.index'));
    }

    /**
     * Display the specified Jenis_hewans.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $jenisHewans = $this->jenisHewansRepository->find($id);

        if (empty($jenisHewans)) {
            Flash::error('Jenis Hewans not found');

            return redirect(route('jenisHewans.index'));
        }

        return view('jenis_hewans.show')->with('jenisHewans', $jenisHewans);
    }

    /**
     * Show the form for editing the specified Jenis_hewans.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $jenisHewans = $this->jenisHewansRepository->find($id);

        if (empty($jenisHewans)) {
            Flash::error('Jenis Hewans not found');

            return redirect(route('jenisHewans.index'));
        }

        return view('jenis_hewans.edit')->with('jenisHewans', $jenisHewans);
    }

    /**
     * Update the specified Jenis_hewans in storage.
     *
     * @param int $id
     * @param UpdateJenis_hewansRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateJenis_hewansRequest $request)
    {
        $jenisHewans = $this->jenisHewansRepository->find($id);

        if (empty($jenisHewans)) {
            Flash::error('Jenis Hewans not found');

            return redirect(route('jenisHewans.index'));
        }

        $jenisHewans = $this->jenisHewansRepository->update($request->all(), $id);

        Flash::success('Jenis Hewans updated successfully.');

        return redirect(route('jenisHewans.index'));
    }

    /**
     * Remove the specified Jenis_hewans from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $jenisHewans = $this->jenisHewansRepository->find($id);

        if (empty($jenisHewans)) {
            Flash::error('Jenis Hewans not found');

            return redirect(route('jenisHewans.index'));
        }

        $this->jenisHewansRepository->delete($id);

        Flash::success('Jenis Hewans deleted successfully.');

        return redirect(route('jenisHewans.index'));
    }
}
