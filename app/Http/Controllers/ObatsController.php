<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateObatsRequest;
use App\Http\Requests\UpdateObatsRequest;
use App\Repositories\ObatsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Models\Satuans;

class ObatsController extends AppBaseController
{
    /** @var  ObatsRepository */
    private $obatsRepository;

    public function __construct(ObatsRepository $obatsRepo)
    {
        $this->obatsRepository = $obatsRepo;
    }

    /**
     * Display a listing of the Obats.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $obats = $this->obatsRepository->all();

        return view('obats.index')
            ->with('obats', $obats);
    }

    /**
     * Show the form for creating a new Obats.
     *
     * @return Response
     */
    public function create()
    {
        $satuan = Satuans::pluck('nama_satuan','id');
        return view('obats.create', compact('satuan'));
    }

    /**
     * Store a newly created Obats in storage.
     *
     * @param CreateObatsRequest $request
     *
     * @return Response
     */
    public function store(CreateObatsRequest $request)
    {
        $input = $request->all();

        $obats = $this->obatsRepository->create($input);

        Flash::success('Obats saved successfully.');

        return redirect(route('obats.index'));
    }

    /**
     * Display the specified Obats.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $obats = $this->obatsRepository->find($id);

        if (empty($obats)) {
            Flash::error('Obats not found');

            return redirect(route('obats.index'));
        }

        return view('obats.show')->with('obats', $obats);
    }

    /**
     * Show the form for editing the specified Obats.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $obats = $this->obatsRepository->find($id);

        if (empty($obats)) {
            Flash::error('Obats not found');

            return redirect(route('obats.index'));
        }

        return view('obats.edit')->with('obats', $obats);
    }

    /**
     * Update the specified Obats in storage.
     *
     * @param int $id
     * @param UpdateObatsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateObatsRequest $request)
    {
        $obats = $this->obatsRepository->find($id);

        if (empty($obats)) {
            Flash::error('Obats not found');

            return redirect(route('obats.index'));
        }

        $obats = $this->obatsRepository->update($request->all(), $id);

        Flash::success('Obats updated successfully.');

        return redirect(route('obats.index'));
    }

    /**
     * Remove the specified Obats from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $obats = $this->obatsRepository->find($id);

        if (empty($obats)) {
            Flash::error('Obats not found');

            return redirect(route('obats.index'));
        }

        $this->obatsRepository->delete($id);

        Flash::success('Obats deleted successfully.');

        return redirect(route('obats.index'));
    }
}
