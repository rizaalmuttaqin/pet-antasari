<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pasiens;
use App\Models\Pemeriksaan;
use App\Models\Dokters;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pasien = Pasiens::all();
        $pasien_count = count($pasien);
        $rekam_medik = Pemeriksaan::all();
        $rekam_medik_count = count($rekam_medik);
        $dokter = Dokters::all();
        $dokter_count = count($dokter);
        return view('home', compact('pasien_count', 'rekam_medik_count', 'dokter_count'));
    }
}
