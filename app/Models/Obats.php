<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Obats
 * @package App\Models
 * @version December 21, 2019, 2:10 pm UTC
 *
 * @property \App\Models\Satuan satuan
 * @property string nama_obat
 * @property string jenis_obat
 * @property string jumlah
 * @property integer satuan_id
 */
class Obats extends Model
{
    use SoftDeletes;

    public $table = 'obat';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'nama_obat',
        'jenis_obat',
        'jumlah',
        'satuan_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nama_obat' => 'string',
        'jenis_obat' => 'string',
        'jumlah' => 'string',
        'satuan_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'satuan_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function satuan()
    {
        return $this->belongsTo(\App\Models\Satuans::class, 'satuan_id');
    }
}
