<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Transaksis
 * @package App\Models
 * @version December 21, 2019, 2:08 pm UTC
 *
 * @property \App\Models\Pasien pasien
 * @property string anamnese
 * @property string pemberian_obat
 * @property string dosis
 * @property string tarif_jasa_dokter
 * @property integer pasien_id
 */
class Transaksis extends Model
{
    use SoftDeletes;

    public $table = 'transaksi';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'anamnese',
        'pemberian_obat',
        'dosis',
        'tarif_jasa_dokter',
        'pasien_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'tgl_pemeriksaan' => 'integer',
        'anamnese' => 'string',
        'pemberian_obat' => 'string',
        'dosis' => 'string',
        'tarif_jasa_dokter' => 'string',
        'pasien_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'pasien_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function pasien()
    {
        return $this->belongsTo(\App\Models\Pasien::class, 'pasien_id');
    }
}
