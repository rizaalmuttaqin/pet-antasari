<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Pendaftarans
 * @package App\Models
 * @version December 21, 2019, 1:40 pm UTC
 *
 * @property \App\Models\Pasien pasien
 * @property string no_antrian
 * @property integer pasien_id
 */
class Pendaftarans extends Model
{
    use SoftDeletes;

    public $table = 'pendaftaran';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'no_antrian',
        'pasien_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'no_antrian' => 'string',
        'pasien_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'pasien_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function pasien()
    {
        return $this->belongsTo(\App\Models\Pasien::class, 'pasien_id');
    }
}
