<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Dokters
 * @package App\Models
 * @version December 18, 2019, 6:03 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection pemeriksaans
 * @property \Illuminate\Database\Eloquent\Collection rekamMediks
 * @property string nama_dokter
 * @property string jabatan
 * @property string alamat
 * @property string no_hp
 */
class Dokters extends Model
{
    use SoftDeletes;

    public $table = 'dokter';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'nama_dokter',
        'jabatan',
        'alamat',
        'no_hp'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nama_dokter' => 'string',
        'jabatan' => 'string',
        'alamat' => 'string',
        'no_hp' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function pemeriksaans()
    {
        return $this->hasMany(\App\Models\Pemeriksaan::class, 'dokter_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function rekamMediks()
    {
        return $this->hasMany(\App\Models\RekamMedik::class, 'dokter_id');
    }
}
