<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Pemeriksaan
 * @package App\Models
 * @version April 22, 2020, 9:50 am UTC
 *
 * @property \App\Models\Dokter dokter
 * @property \App\Models\Pasien pasien
 * @property \Illuminate\Database\Eloquent\Collection rekamMediks
 * @property integer pasien_id
 * @property string pemilk
 * @property string tgl_pemeriksaan
 * @property string anamnese
 * @property string diagnosa
 * @property string tindakan
 * @property string file
 * @property string pemberian_obat
 * @property string dosis
 * @property integer dokter_id
 * @property string tarif_jasa_dokter
 */
class Pemeriksaan extends Model
{
    use SoftDeletes;

    public $table = 'pemeriksaan';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'pasien_id',
        'pemilk',
        'tgl_pemeriksaan',
        'anamnese',
        'diagnosa',
        'tindakan',
        'file',
        'pemberian_obat',
        'dosis',
        'dokter_id',
        'tarif_jasa_dokter'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'pasien_id' => 'integer',
        'pemilk' => 'string',
        'tgl_pemeriksaan' => 'date',
        'anamnese' => 'string',
        'diagnosa' => 'string',
        'tindakan' => 'string',
        'file' => 'string',
        'pemberian_obat' => 'string',
        'dosis' => 'string',
        'dokter_id' => 'integer',
        'tarif_jasa_dokter' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'pasien_id' => 'required',
        'dokter_id' => 'required',
        'file.*'=> 'required|mimes:image/*'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function dokter()
    {
        return $this->belongsTo(\App\Models\Dokters::class, 'dokter_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function pasien()
    {
        return $this->belongsTo(\App\Models\Pasiens::class, 'pasien_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function rekamMediks()
    {
        return $this->hasMany(\App\Models\RekamMedik::class, 'pemeriksaan_id');
    }
}
