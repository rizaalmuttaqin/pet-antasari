<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Satuans
 * @package App\Models
 * @version December 21, 2019, 2:09 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection obats
 * @property string nama
 * @property string keterangan
 */
class Satuans extends Model
{
    use SoftDeletes;

    public $table = 'satuan';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'nama',
        'keterangan'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nama' => 'string',
        'keterangan' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function obats()
    {
        return $this->hasMany(\App\Models\Obat::class, 'satuan_id');
    }
}
