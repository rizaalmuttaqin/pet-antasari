<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Laporan
 * @package App\Models
 * @version September 11, 2020, 1:14 pm UTC
 *
 * @property integer $id
 * @property integer $id_pemilik
 * @property integer $id_pemeriksaan
 * @property integer $id_dokter
 * @property integer $id_user
 * @property integer $id_transaksi
 */
class Laporan extends Model
{
    use SoftDeletes;

    public $table = 'laporan';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'id',
        'id_pemilik',
        'id_pemeriksaan',
        'id_dokter',
        'id_user',
        'id_transaksi'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_pemilik' => 'integer',
        'id_pemeriksaan' => 'integer',
        'id_dokter' => 'integer',
        'id_user' => 'integer',
        'id_transaksi' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_pemilik' => 'required',
        'id_pemeriksaan' => 'required',
        'id_dokter' => 'required',
        'id_user' => 'required',
        'id_transaksi' => 'required',
        'created_at' => 'required',
        'updated_at' => 'required',
        'deleted_at' => 'required'
    ];

    
}
