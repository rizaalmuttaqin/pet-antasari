<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Jenis_hewans
 * @package App\Models
 * @version December 21, 2019, 1:55 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection pasiens
 * @property string jenis_hewan
 */
class Jenis_hewans extends Model
{
    use SoftDeletes;

    public $table = 'jenis_hewan';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'jenis_hewan'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'jenis_hewan' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function pasiens()
    {
        return $this->hasMany(\App\Models\Pasien::class, 'jenis_hewan_id');
    }
}
