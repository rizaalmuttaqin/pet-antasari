<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Pasiens
 * @package App\Models
 * @version December 21, 2019, 1:49 pm UTC
 *
 * @property \App\Models\JenisHewan jenisHewan
 * @property \App\Models\Pemilik pemilik
 * @property \Illuminate\Database\Eloquent\Collection pemeriksaans
 * @property \Illuminate\Database\Eloquent\Collection pendaftarans
 * @property \Illuminate\Database\Eloquent\Collection rekamMediks
 * @property \Illuminate\Database\Eloquent\Collection transaksis
 * @property string nama_pasien
 * @property string file
 * @property integer pemilik_id
 * @property string berat_badan
 * @property string jenis_kelamin
 * @property string ras
 * @property integer jenis_hewan_id
 */
class Pasiens extends Model
{
    use SoftDeletes;

    public $table = 'pasien';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'nama_pasien',
        'file',
        'nama_pemilik',
        'alamat',
        'no_hp',
        'berat_badan',
        'jenis_kelamin',
        'ras',
        'jenis_hewan_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nama_pasien' => 'string',
        'file' =>'string',
        'nama_pemilik' => 'string',
        'alamat'=> 'string',
        'no_hp' => 'string',
        'berat_badan' => 'string',
        'jenis_kelamin' => 'string',
        'ras' => 'string',
        'jenis_hewan_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        //'pemilik_id' => 'required',
        'jenis_hewan_id' => 'required',
        'file.*'=> 'required|mimes:image/*'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function jenisHewan()
    {
        return $this->belongsTo(\App\Models\Jenis_hewans::class, 'jenis_hewan_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function pemilik()
    {
        return $this->belongsTo(\App\Models\Pemiliks::class, 'pemilik_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function pemeriksaans()
    {
        return $this->hasMany(\App\Models\Pemeriksaan::class, 'pasien_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function pendaftarans()
    {
        return $this->hasMany(\App\Models\Pendaftaran::class, 'pasien_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function rekamMediks()
    {
        return $this->hasMany(\App\Models\RekamMedik::class, 'pasien_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function transaksis()
    {
        return $this->hasMany(\App\Models\Transaksi::class, 'pasien_id');
    }
}
