<!-- Pasien Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pasien_id', 'Pasien Id:') !!}
    {!! Form::select('pasien_id',$pasien, null, ['class' => 'form-control']) !!}
</div>

<!-- Diagnosa Field -->
<div class="form-group col-sm-6">
    {!! Form::label('diagnosa', 'Diagnosa:') !!}
    {!! Form::text('diagnosa', null, ['class' => 'form-control']) !!}
</div>

<!-- Dokter Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dokter_id', 'Dokter Id:') !!}
    {!! Form::select('dokter_id',$dokter, null, ['class' => 'form-control']) !!}
</div>

<!-- Tgl Pemeriksaan Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tgl_pemeriksaan', 'Tgl Pemeriksaan:') !!}
    {!! Form::date('tgl_pemeriksaan', null, ['class' => 'form-control','id'=>'tgl_pemeriksaan']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#tgl_pemeriksaan').datetimepicker({
            format: 'YYYY-MM-DD',
        })
    </script>
@endsection

<!-- Pemeriksaan Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pemeriksaan_id', 'Pemeriksaan Id:') !!}
    {!! Form::number('pemeriksaan_id',null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('rekamMediks.index') !!}" class="btn btn-default">Cancel</a>
</div>
