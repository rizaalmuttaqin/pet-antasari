<!-- Pasien Id Field -->
<div class="form-group">
    {!! Form::label('pasien_id', 'Pasien Id:') !!}
    <p>{!! $rekamMedik->pasien_id !!}</p>
</div>

<!-- Diagnosa Field -->
<div class="form-group">
    {!! Form::label('diagnosa', 'Diagnosa:') !!}
    <p>{!! $rekamMedik->diagnosa !!}</p>
</div>

<!-- Dokter Id Field -->
<div class="form-group">
    {!! Form::label('dokter_id', 'Dokter Id:') !!}
    <p>{!! $rekamMedik->dokter_id !!}</p>
</div>

<!-- Tgl Pemeriksaan Field -->
<div class="form-group">
    {!! Form::label('tgl_pemeriksaan', 'Tgl Pemeriksaan:') !!}
    <p>{!! $rekamMedik->tgl_pemeriksaan !!}</p>
</div>

<!-- Pemeriksaan Id Field -->
<div class="form-group">
    {!! Form::label('pemeriksaan_id', 'Pemeriksaan Id:') !!}
    <p>{!! $rekamMedik->pemeriksaan_id !!}</p>
</div>

