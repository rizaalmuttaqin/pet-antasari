<div class="table-responsive">
    <table class="table" id="rekamMediks-table">
        <thead>
            <tr>
                <th>Pasien Id</th>
        <th>Diagnosa</th>
        <th>Dokter Id</th>
        <th>Tgl Pemeriksaan</th>
        <th>Pemeriksaan Id</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($rekamMediks as $rekamMedik)
            <tr>
                <td>{!! $rekamMedik->pasien_id !!}</td>
            <td>{!! $rekamMedik->diagnosa !!}</td>
            <td>{!! $rekamMedik->dokter_id !!}</td>
            <td>{!! $rekamMedik->tgl_pemeriksaan !!}</td>
            <td>{!! $rekamMedik->pemeriksaan_id !!}</td>
                <td>
                    {!! Form::open(['route' => ['rekamMediks.destroy', $rekamMedik->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('rekamMediks.show', [$rekamMedik->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('rekamMediks.edit', [$rekamMedik->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
