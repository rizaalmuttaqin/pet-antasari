@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Rekam Medik
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($rekamMedik, ['route' => ['rekamMediks.update', $rekamMedik->id], 'method' => 'patch']) !!}

                        @include('rekam_mediks.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection