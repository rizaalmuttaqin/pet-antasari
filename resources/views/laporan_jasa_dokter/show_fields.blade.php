<!-- Pasien Id Field -->
<div class="form-group">
    {!! Form::label('pasien_id', 'Pasien Id:') !!}
    <p>{!! $pemeriksaan->pasien->nama_pasien !!}</p>
</div>

<!-- Pemilk Field -->
<div class="form-group">
    {!! Form::label('pasien_id', 'Pemilik :') !!}
    <p>{!! $pemeriksaan->pasien->nama_pemilik !!}</p>
</div>

<!-- Tgl Pemeriksaan Field -->
<div class="form-group">
    {!! Form::label('tgl_pemeriksaan', 'Tgl Pemeriksaan:') !!}
    <p>{!! $pemeriksaan->tgl_pemeriksaan !!}</p>
</div>

<!-- Anamnese Field -->
<div class="form-group">
    {!! Form::label('anamnese', 'Anamnese:') !!}
    <p>{!! $pemeriksaan->anamnese !!}</p>
</div>

<!-- Diagnosa Field -->
<div class="form-group">
    {!! Form::label('diagnosa', 'Diagnosa:') !!}
    <p>{!! $pemeriksaan->diagnosa !!}</p>
</div>

<!-- Tindakan Field -->
<div class="form-group">
    {!! Form::label('tindakan', 'Tindakan:') !!}
    <p>{!! $pemeriksaan->tindakan !!}</p>
</div>

<!-- Foto Field -->
<div class="form-group row mb-1">
    {!! Form::label('file', 'Foto',['class' => 'col-md-3  label-control ']) !!}
    <div class="col-md-9">
        <p class="form-control">{!! $pemeriksaan->file !!}</p>
        <img width="500px" src="{{url('/'.$pemeriksaan->file)}}">
    </div>
</div>

<!-- Pemberian Obat Field -->
<div class="form-group">
    {!! Form::label('pemberian_obat', 'Pemberian Obat:') !!}
    <p>{!! $pemeriksaan->pemberian_obat !!}</p>
</div>

<!-- Dosis Field -->
<div class="form-group">
    {!! Form::label('dosis', 'Dosis:') !!}
    <p>{!! $pemeriksaan->dosis !!}</p>
</div>

<!-- Dokter Id Field -->
<div class="form-group">
    {!! Form::label('dokter_id', 'Nama Dokter :') !!}
    <p>{!! $pemeriksaan->dokter->nama_dokter !!}</p>
</div>

<!-- Tarif Jasa Dokter Field -->
<div class="form-group">
    {!! Form::label('tarif_jasa_dokter', 'Tarif Jasa Dokter:') !!}
    <p>{!! $pemeriksaan->tarif_jasa_dokter !!}</p>
</div>

