<div class="table-responsive">
    <table id="pemeriksaan_table" class="table" data-page-length='10'>
        <thead>
        <tr>
            <th>No.</th>
            <th>Dokter</th>
            <th>Jumlah Pasien Ditangani</th>
            <th>Tarif Jasa Dokter</th>
        </tr>
        </thead>
        <tbody>
        @php
        $no=1;
        @endphp
        @foreach($pemeriksaans as $pemeriksaan)
            <tr>
                <td>{{$no++}}</td>
                <td>{!! $pemeriksaan->dokter->nama_dokter !!}</td>
                <td>{!! $pemeriksaan->jumlah_pasien !!}</td>
                <td>{!! $pemeriksaan->pendapatan !!}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @section('scripts')
    <script>
        $(document).ready(function() {
            $('#pemeriksaan_table').DataTable();
        } );
    </script>
@endsection
</div>
