<form action="{{url('laporan_jasa_dokter')}}" method="get">
    <div class="row input-daterange">
        <div class="col-md-4">
            <input type="number" max=12 min=1 name="bulan" id="month" class="form-control" placeholder="Bulan" >
        </div>
        <div class="col-md-4">
            <input type="number" name="tahun" id="year" class="form-control" placeholder="Tahun" >
        </div>
        <div class="col-md-4">
            <button type="submit" class="btn btn-primary">Filter</button>
            {{--<button type="button" name="refresh" id="refresh" class="btn btn-default position-right-0 align-content-md-end">Refresh</button>--}}
        </div>
    </div>
</form>
<hr>
