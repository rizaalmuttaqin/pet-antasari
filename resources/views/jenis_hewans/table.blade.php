<div class="table-responsive">
    <table class="table" id="jenisHewans-table">
        <thead>
            <tr>
                <th>Jenis Hewan</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($jenisHewans as $jenisHewans)
            <tr>
                <td>{!! $jenisHewans->jenis_hewan !!}</td>
                <td>
                    {!! Form::open(['route' => ['jenisHewans.destroy', $jenisHewans->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('jenisHewans.show', [$jenisHewans->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('jenisHewans.edit', [$jenisHewans->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
