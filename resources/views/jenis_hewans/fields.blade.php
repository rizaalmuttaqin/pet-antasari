<!-- Jenis Hewan Field -->
<div class="form-group col-sm-6">
    {!! Form::label('jenis_hewan', 'Jenis Hewan:') !!}
    {!! Form::text('jenis_hewan', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('jenisHewans.index') !!}" class="btn btn-default">Cancel</a>
</div>
