@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Jenis Hewans
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($jenisHewans, ['route' => ['jenisHewans.update', $jenisHewans->id], 'method' => 'patch']) !!}

                        @include('jenis_hewans.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection