<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="">
	<meta name="description" content="">
	<!-- SITE TITLE -->
	<title>Klinik Hewan Antasari Samarinda</title>

	<!-- STYLESHEETS -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/templatemo-style.css">
	<link href='//fonts.googleapis.com/css?family=Raleway:400,300,600,700' rel='stylesheet' type='text/css'>
<!-- 

Ultra Profile

https://templatemo.com/tm-464-ultra-profile

-->
</head>
<body data-spy="scroll" data-target="#rock-navigation">
	<!-- START NAVIGATION -->
	<div class="navbar navbar-default bs-dos-nav navbar-fixed-top sticky-navigation" role="navigation">
		<div class="container">

			<div class="navbar-header">
				<button class="navbar-toggle" data-toggle="collapse" data-target="#rock-navigation">
					<span class="icon icon-bar"></span>
					<span class="icon icon-bar"></span>
					<span class="icon icon-bar"></span>
				</button>
				<a href="#" class="navbar-brand">Antasari</a>
			</div>
			<nav class="collapse navbar-collapse" id="rock-navigation">
				<ul class="nav navbar-nav navbar-right main-navigation text-uppercase">
					<li><a href="#home" class="smoothScroll">Home</a></li>
					<li><a href="#work" class="smoothScroll">My Work</a></li>
					<li><a href="#portfolio" class="smoothScroll">Portfolio</a></li>
					<li><a href="#resume" class="smoothScroll">Resume</a></li>
					<li><a href="#about" class="smoothScroll">About</a></li>
					<li><a href="#contact" class="smoothScroll">Contact</a></li>
				</ul>
			</nav>

		</div>
	</div>
	<!-- END NAVIGATION -->

	<!-- START HOME -->
	<section id="home" class="templatemo-home">
		<div class="container">
			<div class="row">
				<div class="col-md-2 col-sm-1"></div>
				<div class="col-md-8 col-sm-10">
					<h1 class="tm-home-title"><strong>Antasari Pet</strong></h1>
					<h2 class="tm-home-subtitle"></h2>
					<p><strong>. Klinik Hewan Antasari berdiri sejak 1 April 2018.</strong> <strong>klinik ini melayani berbagai pemeriksaan seperti vaksinasi, pengobatan biasa,steril/kebiri,operasi pada hewan,dan grooming.</strong>  
					<p><strong>Klinik hewan Antasari buka mulai jam 9 pagi sampai jam 10 malam, dan di klinik tersebut mempunyai 3 dokter hewan dan 1 paramedis</strong></p>
					<a href="{{ route('login') }}" class="btn btn-default smoothScroll tm-view-more-btn">Login</a>
					<a href="{{ route('register') }}" class="btn btn-default smoothScroll tm-view-more-btn">Register</a>
				</div>
				<div class="col-md-2 col-sm-1"></div>
			</div>
		</div>
	</section>
	<!-- END HOME -->

	<!-- START work -->
	<section id="work" class="tm-padding-top-bottom-100">
		<div class="container">
			<div class="row">
				<div class="col-md-offset-1 col-md-11">
					<h2 class="title">Visi <strong>Misi</strong></h2>						
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="work-wrapper">
						<i class="fa fa-link"></i>
						<h3 class="text-uppercase tm-work-h3">Visi</h3>
						<hr>
						<p>Menjadi Klinik hewn yang memiliki layan terbaik se-Samarinda dan sekitarnya</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="work-wrapper">
						<i class="fa fa-flash"></i>
						<h3 class="text-uppercase tm-work-h3">Misi</h3>
						<hr>
						<p>Klinik hewan yang didukung dengan tenaga vet profesional</p>
						<p>Klinik hewan yang mengutamakan keselamatan pasien dan melayani secara <strong>anaimal welfare</strong></P>
						<p>Melayani dengan cepat, tepat, dan terpercaya</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="work-wrapper">
						<i class="fa fa-dashboard"></i>
						<h3 class="text-uppercase tm-work-h3">Moto</h3>
						<hr>
						<p>Bekerja dengan tulus dengan mengedepankan keselematan hewan</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- END work -->

	<!-- START PORTFOLIO -->
	<section id="portfolio" class="tm-portfolio">
		<div class="container">
			<div class="row">
				<div class="col-md-12 wow bounce">
					<div class="title">
						<h2 class="tm-portfolio-title"><strong>Galeri</strong></h2>
					</div>

					<!-- START ISO SECTION -->
					<div class="iso-section">
						<ul class="filter-wrapper clearfix">
							<li><a href="#" class="opc-main-bg selected" data-filter="*">All</a></li>
							<li><a href="#" class="opc-main-bg" data-filter=".html">Dokter</a></li>
							<li><a href="#" class="opc-main-bg" data-filter=".photoshop">Team</a></li>
							<li><a href="#" class="opc-main-bg" data-filter=".wordpress">Kasus</a></li>
							<li><a href="#" class="opc-main-bg" data-filter=".mobile">Magang/Pkl</a></li>
						</ul>
						<div class="iso-box-section">
							<div class="iso-box-wrapper col4-iso-box">
								<div class="iso-box html photoshop wordpress mobile col-md-3 col-sm-3 col-xs-12">
									<div class="portfolio-thumb">
										<img src="images/husky.jpg" class="fluid-img" alt="dokter img">
										<div class="portfolio-overlay">
											<h3 class="portfolio-item-title">UX Design</h3>
											<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonumm.</p>
										</div>
									</div>
								</div>
								<div class="iso-box html wordpress mobile col-md-3 col-sm-3 col-xs-12">
									<div class="portfolio-thumb">
										<img src="images/team1.jpg" class="fluid-img" alt="portfolio img">
										<div class="portfolio-overlay">
											<h3 class="portfolio-item-title">UX Design</h3>
											<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonumm.</p>
										</div>
									</div>
								</div>
								<div class="iso-box wordpress col-md-3 col-sm-3 col-xs-12">
									<div class="portfolio-thumb">
										<img src="images/case1.jpg" class="fluid-img" alt="portfolio img">
										<div class="portfolio-overlay">
											<h3 class="portfolio-item-title">UX Design</h3>
											<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonumm.</p>
										</div>
									</div>
								</div>
								<div class="iso-box html mobile col-md-3 col-sm-3 col-xs-12">
									<div class="portfolio-thumb">
										<img src="images/case2.jpg" class="fluid-img" alt="portfolio img">
										<div class="portfolio-overlay">
											<h3 class="portfolio-item-title">UX Design</h3>
											<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonumm.</p>
										</div>
									</div>
								</div>
								<div class="iso-box wordpress col-md-3 col-sm-3 col-xs-12">
									<div class="portfolio-thumb">
										<img src="images/case3.jpg" class="fluid-img" alt="portfolio img">
										<div class="portfolio-overlay">
											<h3 class="portfolio-item-title">UX Design</h3>
											<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonumm.</p>
										</div>
									</div>
								</div>
								<div class="iso-box html photoshop col-md-3 col-sm-3 col-xs-12">
									<div class="portfolio-thumb">
										<img src="images/dokter1.jpg" class="fluid-img" alt="portfolio img">
										<div class="portfolio-overlay">
											<h3 class="portfolio-item-title">UX Design</h3>
											<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonumm.</p>
										</div>
									</div>
								</div>
								<div class="iso-box photoshop col-md-3 col-sm-3 col-xs-12">
									<div class="portfolio-thumb">
										<img src="images/case5.jpg" class="fluid-img" alt="portfolio img">
										<div class="portfolio-overlay">
											<h3 class="portfolio-item-title">UX Design</h3>
											<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonumm.</p>
										</div>
									</div>
								</div>
								<div class="iso-box wordpress col-md-3 col-sm-3 col-xs-12">
									<div class="portfolio-thumb">
										<img src="images/usg.jpg" class="fluid-img" alt="portfolio img">
										<div class="portfolio-overlay">
											<h3 class="portfolio-item-title">UX Design</h3>
											<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonumm.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- END PORTFOLIO -->

	<!-- START RESUME -->
	<section id="resume" class="tm-padding-top-bottom-100">
		<div class="container">
			<div class="row">
				<div class="col-md-5 col-sm-5">					
					<h2 class="title">My <strong>Profile</strong></h2>
					<p><span class="tm-info-label">Name</span> Intan Purwa Dewantari</p>
					<p><span class="tm-info-label">Birthday</span> Mei 2, 1989</p>
					<p><span class="tm-info-label">Address</span> Sempaja Lestari Indah Blok C 77 Rt.16 Samarinda Kaltim</p>
					<p><span class="tm-info-label">Phone</span> 08115555089</p>
					<p><span class="tm-info-label">Email</span> -</p>
					<p><span class="tm-info-label">Website</span> <a href="#" class="tm-red-text">www.company.com</a></p>
				</div>
				
	</section>
	<!-- END RESUME -->
  
	<!-- START ABOUT -->
	<section id="about" class="tm-about">
		<div class="container">
			<div class="row">
				<div class="col-md-offset-5 col-md-5 col-sm-offset-5 col-sm-6">
					<div class="title">
						<h2>This is <strong>me</strong></h2>
						<h1 class="tm-red-text">Creative <strong>Director</strong></h1>
					</div>
					<p>This is free Bootstrap v3.3.4 mobile friendly layout from <a rel="nofollow" href="https://templatemo.com">templatemo</a>. Feel free to use it for your website. Credit goes to <a rel="nofollow" href="https://pixabay.com">Pixabay</a> for photos used in this template. Dolore magna aliquam erat volutpat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet.</p>
					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet. Dolore magna aliquam erat volutpat.</p>
				</div>
			</div>
		</div>
	</section>
	<!-- END ABOUT -->

	<!-- START SOCIAL -->
	<section id="social" class="tm-social">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-4 wow rotateInUpLeft" data-wow-delay="0.3s">
					<div class="media facebook">
						<a href="#">
							<div class="media-object pull-left">
								<i class="fa fa-facebook"></i>
							</div>
							<div class="media-body">
								<h4 class="media-heading tm-social-title">Follow me on</h4>
								<h3>Social Facebook</h3>
							</div>
						</a>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 wow rotateInUpLeft" data-wow-delay="0.6s">
					<div class="media twitter">
						<a href="#">
							<div class="media-object pull-left">
								<i class="fa fa-twitter"></i>
							</div>
							<div class="media-body">
								<h4 class="media-heading tm-social-title">Tweet me on</h4>
								<h3>Social Twitter</h3>
							</div>
						</a>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 wow rotateInUpLeft" data-wow-delay="0.9s">
					<div class="media pinterest">
						<a href="#">
							<div class="media-object pull-left">
								<i class="fa fa-pinterest"></i>
							</div>
							<div class="media-body">
								<h4 class="media-heading tm-social-title">Pin me on</h4>
								<h3>Social Pinterest</h3>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- END SOCIAL -->

	<!-- START CONTACT -->
	<section id="contact" class="tm-contact">
		<div class="container">
			<div class="row">
				<div class="col-md-12">					
					<h2 class="title">Drop <strong>me a line</strong></h2>
					<hr>					
				</div>
				<div class="col-md-1 col-sm-1"></div>
				<div class="col-md-10 col-sm-10">
					<form action="#" method="post">
						<div class="col-md-6 col-sm-6">
							<input class="form-control" type="text" placeholder="Your Name">
						</div>
						<div class="col-md-6 col-sm-6">
							<input class="form-control" type="email" placeholder="Your Email">
						</div>
						<div class="col-md-12 col-sm-12">
							<input class="form-control" type="text" placeholder="Your Subject">
							<textarea class="form-control" placeholder="Your Message" rows="6"></textarea>
						</div>
						<div class="col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8">
							<input class="form-control" type="submit" value="SHOOT MESSAGE">
						</div>
					</form>
				</div>
				<div class="col-md-1 col-sm-1"></div>
				<div class="col-md-12 col-sm-12">
					<p>Copyright &copy; 2018 Ultra Profile
                    . design: <a rel="nofollow noopener" href="https://templatemo.com">template mo</a></p>
				</div>
			</div>
		</div>
	</section>
	<!-- END CONTACT -->
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/smoothscroll.js"></script>
	<script src="js/jquery.nav.js"></script>
	<script src="js/isotope.js"></script>
	<script src="js/imagesloaded.min.js"></script>
	<script src="js/custom.js"></script>
</body>
</html>