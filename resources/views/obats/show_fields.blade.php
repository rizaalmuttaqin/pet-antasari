<!-- Nama Obat Field -->
<div class="form-group">
    {!! Form::label('nama_obat', 'Nama Obat:') !!}
    <p>{!! $obats->nama_obat !!}</p>
</div>

<!-- Jenis Obat Field -->
<div class="form-group">
    {!! Form::label('jenis_obat', 'Jenis Obat:') !!}
    <p>{!! $obats->jenis_obat !!}</p>
</div>

<!-- Jumlah Field -->
<div class="form-group">
    {!! Form::label('jumlah', 'Jumlah:') !!}
    <p>{!! $obats->jumlah !!}</p>
</div>

<!-- Satuan Id Field -->
<div class="form-group">
    {!! Form::label('satuan_id', 'Satuan:') !!}
    <p>{!! $obats->satuan['nama_satuan']!!}</p>
</div>

