<div class="table-responsive">
    <table class="table" id="obats-table">
        <thead>
            <tr>
                <th>Nama Obat</th>
        <th>Jenis Obat</th>
        <th>Jumlah</th>
        <th>Satuan</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($obats as $obats)
            <tr>
                <td>{!! $obats->nama_obat !!}</td>
            <td>{!! $obats->jenis_obat !!}</td>
            <td>{!! $obats->jumlah !!}</td>
            <td>{!! $obats->satuan['nama_satuan'] !!}</td>
                <td>
                    {!! Form::open(['route' => ['obats.destroy', $obats->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('obats.show', [$obats->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('obats.edit', [$obats->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
