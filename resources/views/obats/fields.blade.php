<!-- Nama Obat Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nama_obat', 'Nama Obat:') !!}
    {!! Form::text('nama_obat', null, ['class' => 'form-control']) !!}
</div>

<!-- Jenis Obat Field -->
<div class="form-group col-sm-6">
    {!! Form::label('jenis_obat', 'Jenis Obat:') !!}
    {!! Form::text('jenis_obat', null, ['class' => 'form-control']) !!}
</div>

<!-- Jumlah Field -->
<div class="form-group col-sm-6">
    {!! Form::label('jumlah', 'Jumlah:') !!}
    {!! Form::text('jumlah', null, ['class' => 'form-control']) !!}
</div>

<!-- Satuan Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('satuan', 'Satuan Id:') !!}
    <select name="satuan_id"  class="form-control" >
        @foreach($pasien as $item)
            <option value="{{$item->id}}">{{$item->nama_satuan}}</option>
        @endforeach
    </select>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('obats.index') !!}" class="btn btn-default">Cancel</a>
</div>
