<li class="{{ Request::is('pasiens*') ? 'active' : '' }}">
    <a href="{!! route('pasiens.index') !!}"><i class="fa fa-edit"></i><span>Pasien</span></a>
</li>

<li class="{{ Request::is('pemeriksaans*') ? 'active' : '' }}">
    <a href="{!! route('pemeriksaans.index') !!}"><i class="fa fa-edit"></i><span>Rekam Medik</span></a>
</li>

<li class="{{ Request::is('jenisHewans*') ? 'active' : '' }}">
    <a href="{!! route('jenisHewans.index') !!}"><i class="fa fa-edit"></i><span>Jenis Hewan</span></a>
</li>

<li class="{{ Request::is('dokters*') ? 'active' : '' }}">
    <a href="{!! route('dokters.index') !!}"><i class="fa fa-edit"></i><span>Dokter</span></a>
</li>

<li class="{{ Request::is('obats*') ? 'active' : '' }}">
    <a href="{!! route('obats.index') !!}"><i class="fa fa-edit"></i><span>Obats</span></a>
    
<!-- <li class="{{ Request::is('pemiliks*') ? 'active' : '' }}">
    <a href="{!! route('pemiliks.index') !!}"><i class="fa fa-edit"></i><span>Pemiliks</span></a>
</li>
<li class="{{ Request::is('rekamMediks*') ? 'active' : '' }}">
    <a href="{!! route('rekamMediks.index') !!}"><i class="fa fa-edit"></i><span>Rekam Mediks</span></a>
</li>
<li class="{{ Request::is('pendaftarans*') ? 'active' : '' }}">
    <a href="{!! route('pendaftarans.index') !!}"><i class="fa fa-edit"></i><span>Pendaftarans</span></a>
</li>
<li class="{{ Request::is('pemeriksaans*') ? 'active' : '' }}">
    <a href="{!! route('pemeriksaans.index') !!}"><i class="fa fa-edit"></i><span>Pemeriksaans</span></a>
</li>
<li class="{{ Request::is('transakses*') ? 'active' : '' }}">
    <a href="{!! route('transakses.index') !!}"><i class="fa fa-edit"></i><span>Transakses</span></a>
</li>
<li class="{{ Request::is('satuans*') ? 'active' : '' }}">
    <a href="{!! route('satuans.index') !!}"><i class="fa fa-edit"></i><span>Satuans</span></a>
</li>

<li class="{{ Request::is('pemiliks*') ? 'active' : '' }}">
    <a href="{!! route('pemiliks.index') !!}"><i class="fa fa-edit"></i><span>Pemilik</span></a>
</li>
</li> 
<li class="{{ Request::is('pemiliks*') ? 'active' : '' }}">
    <a href="{!! route('pemiliks.index') !!}"><i class="fa fa-edit"></i><span>Pemiliks</span></a>
</li> --><li class="{{ Request::is('laporan_jasa_dokter*') ? 'active' : '' }}">
    <a href="{{ url('/laporan_jasa_dokter') }}"><i class="fa fa-edit"></i><span>Laporan Jasa Dokter</span></a>
</li>

