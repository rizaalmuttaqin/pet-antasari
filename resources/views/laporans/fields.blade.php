<!-- Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id', 'Id:') !!}
    {!! Form::number('id', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Pemilik Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_pemilik', 'Id Pemilik:') !!}
    {!! Form::number('id_pemilik', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Pemeriksaan Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_pemeriksaan', 'Id Pemeriksaan:') !!}
    {!! Form::number('id_pemeriksaan', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Dokter Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_dokter', 'Id Dokter:') !!}
    {!! Form::number('id_dokter', null, ['class' => 'form-control']) !!}
</div>

<!-- Id User Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_user', 'Id User:') !!}
    {!! Form::number('id_user', null, ['class' => 'form-control']) !!}
</div>

<!-- Id Transaksi Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_transaksi', 'Id Transaksi:') !!}
    {!! Form::number('id_transaksi', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('laporans.index') }}" class="btn btn-default">Cancel</a>
</div>
