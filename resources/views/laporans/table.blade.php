<div class="table-responsive">
    <table class="table" id="laporans-table">
        <thead>
            <tr>
                <th>Dokter</th>
                <th>Pendapatan</th>
            </tr>
        </thead>
        <tbody>
        @foreach($laporans as $laporan)
            <tr>
                <td>{{ $laporan->id }}</td>
            <td>{{ $laporan->id_pemilik }}</td>
            <td>{{ $laporan->id_pemeriksaan }}</td>
            <td>{{ $laporan->id_dokter }}</td>
            <td>{{ $laporan->id_user }}</td>
            <td>{{ $laporan->id_transaksi }}</td>
                <td>
                    {!! Form::open(['route' => ['laporans.destroy', $laporan->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('laporans.show', [$laporan->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('laporans.edit', [$laporan->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
