<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $laporan->id }}</p>
</div>

<!-- Id Pemilik Field -->
<div class="form-group">
    {!! Form::label('id_pemilik', 'Id Pemilik:') !!}
    <p>{{ $laporan->id_pemilik }}</p>
</div>

<!-- Id Pemeriksaan Field -->
<div class="form-group">
    {!! Form::label('id_pemeriksaan', 'Id Pemeriksaan:') !!}
    <p>{{ $laporan->id_pemeriksaan }}</p>
</div>

<!-- Id Dokter Field -->
<div class="form-group">
    {!! Form::label('id_dokter', 'Id Dokter:') !!}
    <p>{{ $laporan->id_dokter }}</p>
</div>

<!-- Id User Field -->
<div class="form-group">
    {!! Form::label('id_user', 'Id User:') !!}
    <p>{{ $laporan->id_user }}</p>
</div>

<!-- Id Transaksi Field -->
<div class="form-group">
    {!! Form::label('id_transaksi', 'Id Transaksi:') !!}
    <p>{{ $laporan->id_transaksi }}</p>
</div>

