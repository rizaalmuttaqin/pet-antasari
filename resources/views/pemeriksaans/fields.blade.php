<!-- Pasien Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pasien_id', 'Pasien Id:') !!}
    {!! Form::select('pasien_id', $pasien, null, ['class' => 'form-control',
    'placeholder'=>'Pilih Pasien']) !!}
</div>

<!-- Pemilk Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pasien_id', 'Pemilik :') !!}
    {!! Form::select('pasien_id', $pemilik, null, ['class' => 'form-control', 'placeholder'=>
    'Pilih Pemilik']) !!}
</div>

<!-- Tgl Pemeriksaan Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tgl_pemeriksaan', 'Tgl Pemeriksaan:') !!}
    <!-- {!! Form::date('tgl_pemeriksaan', null, ['class' => 'form-control','id'=>'tgl_pemeriksaan']) !!} -->
    {!! Form::date('tgl_pemeriksaan', isset($pemeriksaan)&&!is_null($pemeriksaan->tgl_pemeriksaan)?$pemeriksaan->tgl_pemeriksaan->format('Y-m-d'):null, ['class' => 'form-control']) !!}
</div>

<!-- Anamnese Field -->
<div class="form-group col-sm-6">
    {!! Form::label('anamnese', 'Anamnese:') !!}
    {!! Form::text('anamnese', null, ['class' => 'form-control']) !!}
</div>

<!-- Diagnosa Field -->
<div class="form-group col-sm-6">
    {!! Form::label('diagnosa', 'Diagnosa:') !!}
    {!! Form::text('diagnosa', null, ['class' => 'form-control']) !!}
</div>

<!-- Tindakan Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tindakan', 'Tindakan:') !!}
    {!! Form::text('tindakan', null, ['class' => 'form-control']) !!}
</div>

<!-- Foto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('file', 'Foto:') !!}
    {!! Form::file('file', ['class' => 'form-control']) !!}
</div>

<!-- Pemberian Obat Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pemberian_obat', 'Pemberian Obat:') !!}
    {!! Form::text('pemberian_obat', null, ['class' => 'form-control']) !!}
</div>

<!-- Dosis Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dosis', 'Dosis:') !!}
    {!! Form::text('dosis', null, ['class' => 'form-control']) !!}
</div>

<!-- Dokter Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dokter_id', 'Dokter Id:') !!}
    {!! Form::select('dokter_id', $dokter, null, ['class' => 'form-control',
    'placeholder'=>"Piih Dokter"]) !!}
</div>

<!-- Tarif Jasa Dokter Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tarif_jasa_dokter', 'Tarif Jasa Dokter:') !!}
    {!! Form::text('tarif_jasa_dokter', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('pemeriksaans.index') !!}" class="btn btn-default">Cancel</a>
</div>
