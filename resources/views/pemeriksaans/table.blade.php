<div class="table-responsive">
    <table id="pemeriksaan_table" class="table" data-page-length='10'>
        <thead>
        <tr>
        <th>No.</th>
        <th>Pasien</th>
        <th>Pemilk</th>
        <th>Tgl Pemeriksaan</th>
        <th>Anamnese</th>
        <th>Diagnosa</th>
        <th>Tindakan</th>
        <th>Foto</th>
        <th>Pemberian Obat</th>
        <th>Dosis</th>
        <th>Dokter Id</th>
        <th>Tarif Jasa Dokter</th>
         <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @php
        $no=1;
        @endphp
        @foreach($pemeriksaans as $pemeriksaan)
            <tr>
            <td>{{$no++}}</td>
                <td>{!! $pemeriksaan->pasien->nama_pasien !!}</td>
                <td>{!! $pemeriksaan->pasien->nama_pemilik !!}</td>
                <td>{!! $pemeriksaan->tgl_pemeriksaan->toDateString() !!}</td>
                <td>{!! $pemeriksaan->anamnese !!}</td>
                <td>{!! $pemeriksaan->diagnosa !!}</td>
                <td>{!! $pemeriksaan->tindakan !!}</td>
                <td><img width="150px" src="{{url('/'.$pemeriksaan->file)}}"></td>
                <td>{!! $pemeriksaan->pemberian_obat !!}</td>
                <td>{!! $pemeriksaan->dosis !!}</td>
                <td>{!! $pemeriksaan->dokter->nama_dokter !!}</td>
                <td>{!! $pemeriksaan->tarif_jasa_dokter !!}</td>
                <td>
                    {!! Form::open(['route' => ['pemeriksaans.destroy', $pemeriksaan->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('pemeriksaans.show', [$pemeriksaan->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('pemeriksaans.edit', [$pemeriksaan->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @section('scripts')
    <script>
        $(document).ready(function() {
            $('#pemeriksaan_table').DataTable();
        } );
    </script>
@endsection
</div>
