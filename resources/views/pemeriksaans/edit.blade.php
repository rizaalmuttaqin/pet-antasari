@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Pemeriksaan
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($pemeriksaan, ['route' => ['pemeriksaans.update', $pemeriksaan->id], 'method' => 'patch', 
                   'files' => 'true']) !!}

                        @include('pemeriksaans.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection