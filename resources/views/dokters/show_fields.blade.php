<!-- Nama Dokter Field -->
<div class="form-group">
    {!! Form::label('nama_dokter', 'Nama Dokter:') !!}
    <p>{!! $dokters->nama_dokter !!}</p>
</div>

<!-- Jabatan Field -->
<div class="form-group">
    {!! Form::label('jabatan', 'Jabatan:') !!}
    <p>{!! $dokters->jabatan !!}</p>
</div>

<!-- Alamat Field -->
<div class="form-group">
    {!! Form::label('alamat', 'Alamat:') !!}
    <p>{!! $dokters->alamat !!}</p>
</div>

<!-- No Hp Field -->
<div class="form-group">
    {!! Form::label('no_hp', 'No Hp:') !!}
    <p>{!! $dokters->no_hp !!}</p>
</div>

