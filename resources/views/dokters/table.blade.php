<div class="table-responsive">
    <table class="table" id="dokters-table">
        <thead>
            <tr>
                <th>Nama Dokter</th>
        <th>Jabatan</th>
        <th>Alamat</th>
        <th>No Hp</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($dokters as $dokters)
            <tr>
                <td>{!! $dokters->nama_dokter !!}</td>
            <td>{!! $dokters->jabatan !!}</td>
            <td>{!! $dokters->alamat !!}</td>
            <td>{!! $dokters->no_hp !!}</td>
                <td>
                    {!! Form::open(['route' => ['dokters.destroy', $dokters->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('dokters.show', [$dokters->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('dokters.edit', [$dokters->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
