@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Dokters
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($dokters, ['route' => ['dokters.update', $dokters->id], 'method' => 'patch']) !!}

                        @include('dokters.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection