<!-- Nama Pasien Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nama_pasien', 'Nama Pasien:') !!}
    {!! Form::text('nama_pasien', null, ['class' => 'form-control']) !!}
</div>

<!-- Foto Field -->
<div class="form-group col-sm-6">
    {!! Form::label('file', 'Foto:') !!}
    {!! Form::file('file',['class' => 'form-control', 'files'=>'true','required']) !!}
</div>

<!-- Nama Pemilik Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nama_pemilik', 'Nama Pemilik:') !!}
    {!! Form::text('nama_pemilik', null, ['class' => 'form-control']) !!}
</div>

<!-- Alamat Field -->
<div class="form-group col-sm-6">
    {!! Form::label('alamat', 'Alamat Pemilik:') !!}
    {!! Form::text('alamat', null, ['class' => 'form-control']) !!}
</div>

<!-- No Hp Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_hp', 'No Hp:') !!}
    {!! Form::number('no_hp', null, ['class' => 'form-control']) !!}
</div>

<!-- Jenis Hewan Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('jenis_hewan_id', 'Jenis Hewan Id:') !!}
    {!! Form::select('jenis_hewan_id', $jenis_hewan, null, ['class' => 'form-control']) !!}
</div>

<!-- Berat Badan Field -->
<div class="form-group col-sm-6">
    {!! Form::label('berat_badan', 'Berat Badan:') !!}
    {!! Form::text('berat_badan', null, ['class' => 'form-control']) !!}
</div> 

<!-- Jenis Kelamin Field -->
<div class="form-group col-sm-6">
    {!! Form::label('jenis_kelamin', 'Jenis Kelamin :') !!}
    {!! Form::text('jenis_kelamin', null, ['class' => 'form-control']) !!}
</div>

<!-- Ras Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ras', 'Ras:') !!}
    {!! Form::text('ras', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('pasiens.index') !!}" class="btn btn-default">Cancel</a>
</div>
