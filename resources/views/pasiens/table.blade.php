<div class="table-responsive">
    <table id="pasien_table" class="table" data-page-length='10'>
        <thead>
            <tr>
                <th>No</td>
                <th>Nama Pasien</th>
                <th>Foto</th>
                <th>Pemilik</th>
                <th>Kontak</th>
                <th>Alamat</th>
                <th>Berat Badan</th>
                <th>Jenis Kelamin</th>
                <th>Ras</th>
                <th>Jenis Hewan Id</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @php
        $no=1;
        @endphp
        @foreach($pasiens as $pasien)
            <tr>
                <td>{{$no++}}</td>
                <td>{!! $pasien->nama_pasien !!}</td>
                <!-- <td>{!! $pasien->file !!}</td> -->
                <td><img width="150px" src="{{url('/storage/foto/'.$pasien->file)}}"></td>
                <td>{!! $pasien->nama_pemilik !!}</td>
                <td>{!! $pasien->no_hp !!}</td>
                <td>{!! $pasien->alamat !!}</td>
                <td>{!! $pasien->berat_badan !!}</td>
                <td>{!! $pasien->jenis_kelamin !!}</td>
                <td>{!! $pasien->ras !!}</td>
                <td>{!! $pasien->jenisHewan->jenis_hewan !!}</td>
                <td>
                    {!! Form::open(['route' => ['pasiens.destroy', $pasien->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('pasiens.show', [$pasien->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('pasiens.edit', [$pasien->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @section('scripts')
    <script>
        $(document).ready(function() {
            $('#pasien_table').DataTable();
        } );
    </script>
@endsection
</div>

