<!-- Nama Pasien Field -->
<div class="form-group">
    {!! Form::label('nama_pasien', 'Nama Pasien:') !!}
    <p>{!! $pasiens->nama_pasien !!}</p>
</div>

<!-- Foto Field -->
<div class="form-group row mb-1">
    {!! Form::label('file', 'Foto',['class' => 'col-md-3  label-control ']) !!}
    <div class="col-md-9">
        <!-- <p class="form-control">{!! $pasiens->file !!}</p> -->
        <img width="250px" src="{{url('/storage/foto/'.$pasiens->file)}}">
    </div>
</div>

<!-- Nama Pemilik Field -->
<div class="form-group">
    {!! Form::label('nama_pemilik', 'Nama Pemilik:') !!}
    <p>{!! $pasiens->nama_pemilik !!}</p>
</div>

<!-- Kontak Field -->
<div class="form-group">
    {!! Form::label('kontak', 'Kontak:') !!}
    <p>{!! $pasiens->no_hp !!}</p>
</div>


<!-- Alamat Field -->
<div class="form-group">
    {!! Form::label('alamat', 'Alamat:') !!}
    <p>{!! $pasiens->alamat !!}</p>
</div>

<!-- Berat Badan Field -->
<div class="form-group">
    {!! Form::label('berat_badan', 'Berat Badan:') !!}
    <p>{!! $pasiens->berat_badan !!}</p>
</div>

<!-- Jenis Kelamin Field -->
<div class="form-group">
    {!! Form::label('jenis_kelamin', 'Jenis Kelamin:') !!}
    <p>{!! $pasiens->jenis_kelamin !!}</p>
</div>

<!-- Ras Field -->
<div class="form-group">
    {!! Form::label('ras', 'Ras:') !!}
    <p>{!! $pasiens->ras !!}</p>
</div>

<!-- Jenis Hewan Id Field -->
<div class="form-group">
    {!! Form::label('jenis_hewan_id', 'Jenis Hewan Id:') !!}
    <p>{!! $pasiens->jenis_hewan_id !!}</p>
</div>

