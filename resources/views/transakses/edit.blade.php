@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Transaksis
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($transaksis, ['route' => ['transakses.update', $transaksis->id], 'method' => 'patch']) !!}

                        @include('transakses.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection