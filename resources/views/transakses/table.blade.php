<div class="table-responsive">
    <table class="table" id="transakses-table">
        <thead>
            <tr>
                <th>Anamnese</th>
        <th>Pemberian Obat</th>
        <th>Dosis</th>
        <th>Tarif Jasa Dokter</th>
        <th>Pasien Id</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($transakses as $transaksis)
            <tr>
                <td>{!! $transaksis->anamnese !!}</td>
            <td>{!! $transaksis->pemberian_obat !!}</td>
            <td>{!! $transaksis->dosis !!}</td>
            <td>{!! $transaksis->tarif_jasa_dokter !!}</td>
            <td>{!! $transaksis->pasien_id !!}</td>
                <td>
                    {!! Form::open(['route' => ['transakses.destroy', $transaksis->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('transakses.show', [$transaksis->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('transakses.edit', [$transaksis->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
