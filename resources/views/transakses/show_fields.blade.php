<!-- Anamnese Field -->
<div class="form-group">
    {!! Form::label('anamnese', 'Anamnese:') !!}
    <p>{!! $transaksis->anamnese !!}</p>
</div>

<!-- Pemberian Obat Field -->
<div class="form-group">
    {!! Form::label('pemberian_obat', 'Pemberian Obat:') !!}
    <p>{!! $transaksis->pemberian_obat !!}</p>
</div>

<!-- Dosis Field -->
<div class="form-group">
    {!! Form::label('dosis', 'Dosis:') !!}
    <p>{!! $transaksis->dosis !!}</p>
</div>

<!-- Tarif Jasa Dokter Field -->
<div class="form-group">
    {!! Form::label('tarif_jasa_dokter', 'Tarif Jasa Dokter:') !!}
    <p>{!! $transaksis->tarif_jasa_dokter !!}</p>
</div>

<!-- Pasien Id Field -->
<div class="form-group">
    {!! Form::label('pasien_id', 'Pasien Id:') !!}
    <p>{!! $transaksis->pasien_id !!}</p>
</div>

