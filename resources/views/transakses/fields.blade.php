<!-- Anamnese Field -->
<div class="form-group col-sm-6">
    {!! Form::label('anamnese', 'Anamnese:') !!}
    {!! Form::text('anamnese', null, ['class' => 'form-control']) !!}
</div>

<!-- Pemberian Obat Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pemberian_obat', 'Pemberian Obat:') !!}
    {!! Form::text('pemberian_obat', null, ['class' => 'form-control']) !!}
</div>

<!-- Dosis Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dosis', 'Dosis:') !!}
    {!! Form::text('dosis', null, ['class' => 'form-control']) !!}
</div>

<!-- Tarif Jasa Dokter Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tarif_jasa_dokter', 'Tarif Jasa Dokter:') !!}
    {!! Form::text('tarif_jasa_dokter', null, ['class' => 'form-control']) !!}
</div>

<!-- Pasien Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pasien_id', 'Pasien Id:') !!}
    {!! Form::select('pasien_id',$pasien, null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('transakses.index') !!}" class="btn btn-default">Cancel</a>
</div>
