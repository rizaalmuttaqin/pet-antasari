<div class="table-responsive">
    <table class="table" id="pendaftarans-table">
        <thead>
            <tr>
                <th>No Antrian</th>
        <th>Pasien Id</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($pendaftarans as $pendaftarans)
            <tr>
                <td>{!! $pendaftarans->no_antrian !!}</td>
            <td>{!! $pendaftarans->pasien_id !!}</td>
                <td>
                    {!! Form::open(['route' => ['pendaftarans.destroy', $pendaftarans->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('pendaftarans.show', [$pendaftarans->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('pendaftarans.edit', [$pendaftarans->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
