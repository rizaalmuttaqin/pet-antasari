@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Pendaftarans
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($pendaftarans, ['route' => ['pendaftarans.update', $pendaftarans->id], 'method' => 'patch']) !!}

                        @include('pendaftarans.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection