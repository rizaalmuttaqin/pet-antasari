<!-- No Antrian Field -->
<div class="form-group col-sm-6">
    {!! Form::label('no_antrian', 'No Antrian:') !!}
    {!! Form::text('no_antrian', null, ['class' => 'form-control']) !!}
</div>

<!-- Pasien Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pasien_id', 'Pasien Id:') !!}
    {!! Form::select('pasien_id', $pasien, null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('pendaftarans.index') !!}" class="btn btn-default">Cancel</a>
</div>
