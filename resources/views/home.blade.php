@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="container-fluid" style="margin-top: 50px">
	        <div class="row">
	            <div class="col-md-3 col-sm-4">
	                <div class="wrimagecard wrimagecard-topimage">
                        <a href="#">
                        <div class="wrimagecard-topimage_header" style="background-color:rgba(187, 120, 36, 0.1) ">
                            <center><img width="150px" src="{{url('/image/medical-records.png')}}"></center>
                        </div>
                        <div class="wrimagecard-topimage_title">
                            <h4>Rekam Medik
                                <div class="pull-right badge">{{$rekam_medik_count}}</div>
                            </h4>
                        </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4">
                    <div class="wrimagecard wrimagecard-topimage">
                        <a href="#">
                        <div class="wrimagecard-topimage_header" style="background-color: rgba(22, 160, 133, 0.1)">
                            <center><img width="150px" src="{{url('/image/patient.png')}}"></center>
                        </div>
                        <div class="wrimagecard-topimage_title">
                            <h4>Pasien
                                <div class="pull-right badge" id="WrControls">{{$pasien_count}}</div>
                            </h4>
                        </div>
                        </a>
                    </div>
                </div>
<div class="col-md-3 col-sm-4">
      <div class="wrimagecard wrimagecard-topimage">
          <a href="#">
          <div class="wrimagecard-topimage_header" style="background-color:  rgba(213, 15, 37, 0.1)">
            <center><img width="150px" src="{{url('/image/doctor.png')}}"></center>
          </div>
          <div class="wrimagecard-topimage_title" >
            <h4>Dokter
            <div class="pull-right badge" id="WrForms">{{$dokter_count}}</div>
            </h4>
          </div>
          
        </a>
      </div>
	</div>
</div>
</div>
    </div>
</div>
@endsection
