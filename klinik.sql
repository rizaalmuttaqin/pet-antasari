-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 30 Okt 2020 pada 15.55
-- Versi server: 10.4.14-MariaDB
-- Versi PHP: 7.3.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `klinik`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `dokter`
--

CREATE TABLE `dokter` (
  `id` int(11) NOT NULL,
  `nama_dokter` varchar(45) DEFAULT NULL,
  `jabatan` varchar(45) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `no_hp` varchar(45) DEFAULT NULL,
  `created_at` varchar(45) DEFAULT NULL,
  `updated_at` varchar(45) DEFAULT NULL,
  `deleted_at` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `dokter`
--

INSERT INTO `dokter` (`id`, `nama_dokter`, `jabatan`, `alamat`, `no_hp`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Drh. Intan Purwa Dewantari', 'Dokter', 'Sempaja Lestari Indah Blok C 77 Rt.16 Samarinda Kaltim', '08115555089', '2020-01-21 09:21:24', '2020-01-21 09:21:24', NULL),
(2, 'vincentia N.A', 'Dokter', 'jl banggeris gg pitbul', '08115528000', '2020-03-02 11:27:00', '2020-03-02 11:27:00', NULL),
(3, 'Rian', 'Dokter', 'Jl M. Yamin', '081240285419', '2020-04-29 06:05:22', '2020-04-29 06:05:22', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_hewan`
--

CREATE TABLE `jenis_hewan` (
  `id` int(11) NOT NULL,
  `jenis_hewan` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `jenis_hewan`
--

INSERT INTO `jenis_hewan` (`id`, `jenis_hewan`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Kucing', '2020-01-21 01:43:27', '2020-10-01 22:59:58', NULL),
(2, 'Anjing', '2020-03-01 21:19:44', '2020-10-01 23:00:12', NULL),
(3, 'Kelinci', '2020-03-01 21:19:56', '2020-10-01 23:00:31', NULL),
(4, 'Ayam', '2020-03-01 21:20:04', '2020-10-01 23:02:34', NULL),
(5, 'Hamster', '2020-03-06 01:33:31', '2020-10-01 23:02:27', NULL),
(6, 'Burung', '2020-03-06 01:33:40', '2020-10-01 23:02:17', NULL),
(7, 'Reptil', '2020-03-06 01:33:48', '2020-10-01 23:02:08', NULL),
(8, 'Monyet', '2020-03-06 01:34:01', '2020-10-01 23:02:00', NULL),
(9, 'Musang', '2020-03-06 01:34:11', '2020-10-01 23:01:52', NULL),
(10, 'Kera', '2020-03-06 01:34:38', '2020-10-01 23:01:42', NULL),
(11, 'Sugar glider', '2020-03-06 01:35:07', '2020-10-01 23:01:32', NULL),
(12, 'Tupai', '2020-03-06 01:35:13', '2020-10-01 23:01:23', NULL),
(13, 'Kambing', '2020-03-06 01:36:20', '2020-10-01 23:01:15', NULL),
(14, 'Bebek', '2020-03-06 01:37:53', '2020-10-01 23:01:06', NULL),
(15, 'Aquatik', '2020-03-06 01:38:09', '2020-10-01 23:00:58', NULL),
(16, 'Kura-kura', '2020-03-06 01:38:49', '2020-10-01 23:00:50', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `laporan`
--

CREATE TABLE `laporan` (
  `id` int(20) UNSIGNED NOT NULL,
  `id_pemilik` int(20) NOT NULL,
  `id_pemeriksaan` int(20) UNSIGNED DEFAULT NULL,
  `id_dokter` int(20) UNSIGNED DEFAULT NULL,
  `id_user` int(20) UNSIGNED DEFAULT NULL,
  `id_transaksi` int(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `obat`
--

CREATE TABLE `obat` (
  `id` int(11) NOT NULL,
  `nama_obat` varchar(45) DEFAULT NULL,
  `jenis_obat` varchar(45) DEFAULT NULL,
  `jumlah` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `satuan_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `obat`
--

INSERT INTO `obat` (`id`, `nama_obat`, `jenis_obat`, `jumlah`, `created_at`, `updated_at`, `deleted_at`, `satuan_id`) VALUES
(1, 'coatex', 'vitamin', '2 box', '2020-03-01 23:08:15', '2020-03-01 23:08:15', NULL, 1),
(2, 'enbatic', 'antibiotik', '1 box', '2020-03-01 23:23:23', '2020-03-01 23:23:23', NULL, 2),
(3, 'simethirone', NULL, '1', '2020-03-02 01:21:40', '2020-03-02 01:21:40', NULL, 1),
(4, 'danzen', NULL, '1', '2020-03-02 01:22:07', '2020-03-02 01:22:07', NULL, 1),
(5, 'digoxin', NULL, NULL, '2020-03-02 01:22:28', '2020-03-02 01:22:28', NULL, 1),
(6, 'genbody', NULL, NULL, '2020-03-02 01:25:51', '2020-03-02 01:25:51', NULL, 4),
(7, 'metronidszole', NULL, NULL, '2020-03-02 02:26:05', '2020-03-02 02:26:05', NULL, 1),
(8, 'dansetion', NULL, NULL, '2020-03-02 02:26:44', '2020-03-02 02:26:44', NULL, 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pasien`
--

CREATE TABLE `pasien` (
  `id` int(11) NOT NULL,
  `nama_pasien` varchar(225) DEFAULT NULL,
  `file` varchar(225) DEFAULT NULL,
  `nama_pemilik` varchar(225) DEFAULT NULL,
  `alamat` varchar(225) DEFAULT NULL,
  `no_hp` varchar(225) DEFAULT NULL,
  `berat_badan` varchar(225) DEFAULT NULL,
  `jenis_kelamin` enum('jantan','betina') DEFAULT NULL,
  `ras` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `jenis_hewan_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pasien`
--

INSERT INTO `pasien` (`id`, `nama_pasien`, `file`, `nama_pemilik`, `alamat`, `no_hp`, `berat_badan`, `jenis_kelamin`, `ras`, `created_at`, `updated_at`, `deleted_at`, `jenis_hewan_id`) VALUES
(258, 'oyen', NULL, 'nabila', 'Jl anggur gg h jamal', '085348323482', '3 kg', 'betina', 'Mixdome', '2020-04-19 04:24:05', '2020-04-19 04:28:38', '2020-04-19 04:28:38', 1),
(259, 'Kitty', NULL, 'bela', 'Jl anggur gg h jamal', '085348323482', '3kg', 'betina', 'Mixdome', '2020-04-19 21:51:20', '2020-07-21 15:27:48', '2020-07-21 15:27:48', 1),
(260, 'Mimi', NULL, 'Erwin', 'jl kadrie oening', '085250796565', '3kg', 'betina', 'Persia', '2020-04-20 01:06:05', '2020-07-21 15:27:53', '2020-07-21 15:27:53', 1),
(261, 'Mimi', NULL, 'elisa', 'Jl anggur gg h jamal', '085345871676', '2,4 Kg', 'betina', 'Mixdome', '2020-06-16 18:17:06', '2020-07-21 15:27:55', '2020-07-21 15:27:55', 1),
(262, 'popi', NULL, 'elisa', 'Jl anggur gg h jamal', '08115555089', '3kg', 'betina', 'Persia', '2020-07-21 14:18:34', '2020-07-21 15:27:57', '2020-07-21 15:27:57', 1),
(263, 'Poppy', NULL, 'elisa', 'jl kadrie oening', '08115555089', '3kg', 'betina', 'Persia', '2020-07-22 05:01:03', '2020-07-23 06:42:07', '2020-07-23 06:42:07', 1),
(264, 'Jack', 'storage/foto/5f7ddac2058e5_5.png', 'nabila', 'Jl anggur gg h jamal', '08115555089', '4,26', 'jantan', 'Persia', '2020-07-23 05:31:04', '2020-10-07 07:12:02', NULL, 1),
(265, 'tamtam', 'storage/foto/5f9c112d0ee43_10.jpg', 'elisa', 'Jl anggur gg h jamal', '0822', '3kg', 'jantan', 'Dome', '2020-09-06 14:13:51', '2020-10-30 05:12:13', NULL, 1),
(266, 'Moza', NULL, 'Aco', 'jl juandaa', '085250796565', NULL, 'jantan', 'mixdome', '2020-09-06 17:04:54', '2020-09-06 17:04:54', NULL, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pemeriksaan`
--

CREATE TABLE `pemeriksaan` (
  `id` int(11) NOT NULL,
  `pasien_id` int(11) NOT NULL,
  `pemilk` varchar(50) DEFAULT NULL,
  `tgl_pemeriksaan` date DEFAULT NULL,
  `anamnese` varchar(225) DEFAULT NULL,
  `diagnosa` varchar(225) DEFAULT NULL,
  `tindakan` varchar(255) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `pemberian_obat` varchar(45) DEFAULT NULL,
  `dosis` varchar(45) DEFAULT NULL,
  `dokter_id` int(11) NOT NULL,
  `tarif_jasa_dokter` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pemeriksaan`
--

INSERT INTO `pemeriksaan` (`id`, `pasien_id`, `pemilk`, `tgl_pemeriksaan`, `anamnese`, `diagnosa`, `tindakan`, `file`, `pemberian_obat`, `dosis`, `dokter_id`, `tarif_jasa_dokter`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 259, NULL, NULL, 'scabies', 'scabies', 'ivomec+dryl', NULL, '-', '-', 1, '100000', '2020-04-19 22:17:28', '2020-04-22 03:05:45', '2020-04-22 03:05:45'),
(2, 260, NULL, '2020-04-20', 'scabies', 'scabies', 'ivomec+dryl', NULL, '-', '-', 1, '100000', '2020-04-20 01:56:28', '2020-04-22 03:05:48', '2020-04-22 03:05:48'),
(4, 259, NULL, '2020-04-22', 'gatal', 'scabies', 'ivomec+dryl', 'd l0.jpg', 'mox, kejibeling', 'iodio= 1x1,hufadon= 1x1,metro= 2x1', 1, '100000', '2020-04-22 03:43:27', '2020-05-03 23:54:50', '2020-05-03 23:54:50'),
(5, 260, NULL, '2020-04-21', 'gatal', 'scabies', 'ivomec+dryl', 'dfd lv 1.jpg', 'sirup', '2x 1/8', 1, '110000', '2020-04-22 04:38:53', '2020-05-03 23:54:57', '2020-05-03 23:54:57'),
(6, 260, NULL, '2020-04-16', 'gatal', 'diare', 'ivomec+dryl', 'd lv 0.jpg', 'sirup', '1 ml 3 kali sehari di oles', 1, '110000', '2020-04-24 22:14:25', '2020-05-03 23:55:00', '2020-05-03 23:55:00'),
(7, 261, NULL, '2020-04-29', 'steril', 'steril', 'steril', 'C:\\xampp\\tmp\\php7428.tmp', '-', '-', 1, '100000', '2020-04-28 21:44:31', '2020-07-21 14:38:03', '2020-07-21 14:38:03'),
(8, 259, NULL, '2020-05-12', 'scabies', 'scabies', 'ivomec+dryl', 'C:\\xampp\\tmp\\php953E.tmp', 'salep', 'iodio= 1x1,hufadon= 1x1,metro= 2x1', 1, '110000', '2020-05-03 22:32:35', '2020-07-21 14:38:06', '2020-07-21 14:38:06'),
(9, 260, NULL, '2020-05-04', 'gatal', 'scabies', 'ivomec+dryl', '(Register Page).jpg', 'salep', '1 ml 3 kali sehari di oles', 2, '110000', '2020-05-03 22:40:41', '2020-07-21 14:38:08', '2020-07-21 14:38:08'),
(10, 260, NULL, '2020-05-04', 'gatal', 'scabies', 'ivomec+dryl', NULL, 'salep', 'iodio= 1x1,hufadon= 1x1,metro= 2x1', 3, '250000', '2020-05-03 22:53:35', '2020-07-21 14:38:10', '2020-07-21 14:38:10'),
(12, 259, NULL, '2020-05-01', 'gatal', 'scabies', 'ivomec+dryl', NULL, 'salep', 'iodio= 1x1,hufadon= 1x1,metro= 2x1', 1, '110000', '2020-05-03 23:53:24', '2020-06-09 20:36:41', '2020-06-09 20:36:41'),
(13, 259, NULL, '2020-05-04', 'gatal', 'scabies', 'ivomec+dryl', 'storage/foto/5eafd1d8e20a9_d l0.jpg', 'salep', '2x 1/8', 2, '110000', '2020-05-04 00:27:05', '2020-06-09 20:36:38', '2020-06-09 20:36:38'),
(14, 260, NULL, '2020-07-03', 'scabies', 'scabies', 'ivomec+dryl', NULL, 'salep', '2x 1/8', 2, '80000', '2020-07-03 05:05:45', '2020-07-21 14:38:13', '2020-07-21 14:38:13'),
(15, 260, NULL, '2020-07-03', '-', 'diare', 'suntik', NULL, 'sirup', '1 ml 3 kali sehari di oles', 1, '110000', '2020-07-03 05:06:31', '2020-07-21 14:38:15', '2020-07-21 14:38:15'),
(16, 259, NULL, '2020-07-03', 'gatal', 'scabies', 'ivomec+dryl', NULL, '-', '-', 3, '80000', '2020-07-03 05:17:31', '2020-07-21 14:38:17', '2020-07-21 14:38:17'),
(17, 260, NULL, '2020-07-03', 'scabies', 'scabies', 'ivomec+dryl', NULL, '-', '-', 2, '80000', '2020-07-03 05:17:59', '2020-07-21 14:38:20', '2020-07-21 14:38:20'),
(18, 259, NULL, '2020-07-03', 'gatal', 'scabies', 'ivomec+dryl', NULL, '-', '-', 2, '80000', '2020-07-03 05:18:37', '2020-07-21 14:38:22', '2020-07-21 14:38:22'),
(19, 261, NULL, '2020-07-03', 'scabies', 'scabies', 'ivomec+dryl', 'cp 1.JPG', '-', '-', 3, '110000', '2020-07-03 05:19:14', '2020-07-21 14:37:59', '2020-07-21 14:37:59'),
(20, 259, NULL, '2020-07-03', 'scabies', 'scabies', 'ivomec+dryl', NULL, '-', '-', 2, '110000', '2020-07-03 05:42:46', '2020-07-21 14:38:24', '2020-07-21 14:38:24'),
(21, 259, NULL, '2020-07-07', 'scabies', 'scabies', 'ivomec+dryl', '', '-', '-', 3, '100000', '2020-07-06 20:52:45', '2020-07-21 14:38:26', '2020-07-21 14:38:26'),
(22, 260, NULL, '2020-07-07', 'gatal', 'scabies', 'ivomec+dryl', 'storage/foto/5f0405776392d_cp 1.JPG', 'salep', '1 ml 3 kali sehari di oles', 1, '100000', '2020-07-06 21:17:43', '2020-07-21 14:38:29', '2020-07-21 14:38:29'),
(23, 259, NULL, '2020-07-19', 'gatal', 'scabies', 'ivomec+dryl', 'storage/foto/5f13f8bcba705_logo clinic 2.jpg', 'salep', '2x 1/8', 1, '250000', '2020-07-18 23:29:33', '2020-07-21 14:42:58', '2020-07-21 14:42:58'),
(24, 264, NULL, '2020-07-28', NULL, 'scabies', 'ivomec+dryl', 'storage/foto/5f1fdf3f11db4_d l0.jpg', '-', '--', 2, '100000', '2020-07-28 00:18:07', '2020-07-28 00:18:07', NULL),
(25, 265, NULL, '2020-09-07', 'scabies', 'scabies', 'ivomec+dryl', 'storage/foto/5f555f64e6342_veter.JPG', '-', '-', 1, '100000', '2020-09-06 14:15:01', '2020-09-06 14:15:01', NULL),
(26, 266, NULL, '2020-09-07', 'steril', 'steril', 'kastrasi', NULL, '-', '-', 1, '100000', '2020-09-06 17:05:59', '2020-09-06 17:05:59', NULL),
(27, 265, NULL, '2020-10-05', 'gatal', 'scabies', 'ivomec+dryl', NULL, '-', '-', 2, '100000', '2020-10-05 06:18:41', '2020-10-05 06:18:41', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pendaftaran`
--

CREATE TABLE `pendaftaran` (
  `id` int(11) NOT NULL,
  `no_antrian` varchar(45) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `pasien_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rekam_medik`
--

CREATE TABLE `rekam_medik` (
  `pasien_id` int(11) NOT NULL,
  `tgl_pemeriksaan` date NOT NULL,
  `diagnosa` varchar(45) DEFAULT NULL,
  `dokter_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `pemeriksaan_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `satuan`
--

CREATE TABLE `satuan` (
  `id` int(11) NOT NULL,
  `nama_satuan` varchar(45) DEFAULT NULL,
  `keterangan` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `satuan`
--

INSERT INTO `satuan` (`id`, `nama_satuan`, `keterangan`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'tablet', 'kapsul', '2020-01-21 00:23:56', '2020-03-01 22:46:43', NULL),
(2, 'sachet', 'serbuk', '2020-03-01 22:46:31', '2020-03-01 22:46:31', NULL),
(3, 'ml', 'cair', '2020-03-01 22:46:58', '2020-03-01 22:46:58', NULL),
(4, 'gr', 'krim/gel', '2020-03-01 22:47:20', '2020-03-01 22:47:20', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `id` int(20) UNSIGNED NOT NULL,
  `tarif_jasa_dokter` varchar(45) DEFAULT NULL,
  `created_at` varchar(45) DEFAULT NULL,
  `updated_at` varchar(45) DEFAULT NULL,
  `deleted_at` varchar(45) DEFAULT NULL,
  `pasien_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Elisa Rahmawati', 'admin@gmail.com', NULL, '$2y$10$zjN6DVYuyQTo9SBFgptjH.Hob6yqxXTQKtHG49UFfbnRD5A.t7DRm', NULL, '2019-12-17 22:09:23', '2019-12-17 22:09:23'),
(2, 'bella varadina', 'bellavaradina@gmail.com', NULL, '$2y$10$NPRSf8lavkZMCVhJHMcv2OHurMhLVbUulzbKJubeF1sjrVhplIp2S', NULL, '2020-03-20 03:17:57', '2020-03-20 03:17:57');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `dokter`
--
ALTER TABLE `dokter`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `jenis_hewan`
--
ALTER TABLE `jenis_hewan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `laporan`
--
ALTER TABLE `laporan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `foreign_user` (`id_user`),
  ADD KEY `foreign_transaksi` (`id_transaksi`),
  ADD KEY `foreign_pemilik` (`id_pemilik`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `obat`
--
ALTER TABLE `obat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_obat_satuan1_idx` (`satuan_id`);

--
-- Indeks untuk tabel `pasien`
--
ALTER TABLE `pasien`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_pasien_jenis_hewan1_idx` (`jenis_hewan_id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `pemeriksaan`
--
ALTER TABLE `pemeriksaan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_pemeriksaan_pasien1_idx` (`pasien_id`),
  ADD KEY `fk_pemeriksaan_dokter1_idx` (`dokter_id`);

--
-- Indeks untuk tabel `pendaftaran`
--
ALTER TABLE `pendaftaran`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_pendaftaran_pasien1_idx` (`pasien_id`);

--
-- Indeks untuk tabel `rekam_medik`
--
ALTER TABLE `rekam_medik`
  ADD PRIMARY KEY (`tgl_pemeriksaan`),
  ADD KEY `fk_rekam_medik_pemeriksaan1_idx` (`pemeriksaan_id`),
  ADD KEY `fk_rekam_medik_pasien1_idx` (`pasien_id`),
  ADD KEY `fk_rekam_medik_dokter1_idx` (`dokter_id`);

--
-- Indeks untuk tabel `satuan`
--
ALTER TABLE `satuan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_transaksi_pasien1_idx` (`pasien_id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `dokter`
--
ALTER TABLE `dokter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `jenis_hewan`
--
ALTER TABLE `jenis_hewan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `laporan`
--
ALTER TABLE `laporan`
  MODIFY `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `pasien`
--
ALTER TABLE `pasien`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=267;

--
-- AUTO_INCREMENT untuk tabel `pemeriksaan`
--
ALTER TABLE `pemeriksaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT untuk tabel `pendaftaran`
--
ALTER TABLE `pendaftaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `satuan`
--
ALTER TABLE `satuan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `laporan`
--
ALTER TABLE `laporan`
  ADD CONSTRAINT `foreign_pemilik` FOREIGN KEY (`id_pemilik`) REFERENCES `pasien` (`id`),
  ADD CONSTRAINT `foreign_transaksi` FOREIGN KEY (`id_transaksi`) REFERENCES `transaksi` (`id`),
  ADD CONSTRAINT `foreign_user` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`);

--
-- Ketidakleluasaan untuk tabel `obat`
--
ALTER TABLE `obat`
  ADD CONSTRAINT `fk_obat_satuan1` FOREIGN KEY (`satuan_id`) REFERENCES `satuan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `pemeriksaan`
--
ALTER TABLE `pemeriksaan`
  ADD CONSTRAINT `fk_pemeriksaan_dokter1` FOREIGN KEY (`dokter_id`) REFERENCES `dokter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pemeriksaan_pasien1` FOREIGN KEY (`pasien_id`) REFERENCES `pasien` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `pendaftaran`
--
ALTER TABLE `pendaftaran`
  ADD CONSTRAINT `fk_pendaftaran_pasien1` FOREIGN KEY (`pasien_id`) REFERENCES `pasien` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `rekam_medik`
--
ALTER TABLE `rekam_medik`
  ADD CONSTRAINT `fk_rekam_medik_dokter1` FOREIGN KEY (`dokter_id`) REFERENCES `dokter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_rekam_medik_pasien1` FOREIGN KEY (`pasien_id`) REFERENCES `pasien` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_rekam_medik_pemeriksaan1` FOREIGN KEY (`pemeriksaan_id`) REFERENCES `pemeriksaan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `fk_transaksi_pasien1` FOREIGN KEY (`pasien_id`) REFERENCES `pasien` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
